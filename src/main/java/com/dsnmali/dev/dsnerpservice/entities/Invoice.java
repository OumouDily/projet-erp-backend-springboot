/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.entities.parents.InvoiceParentEntity;
import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author meididiallo
 */
@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "invoice")
public class Invoice extends InvoiceParentEntity implements Serializable {

    @Column(name = "reference")
    private String reference;
    @Column(name = "delivery_delay")
    private String deliveryDelay;
    @Column(name = "method_of_payment")
    @Enumerated(EnumType.STRING)
    private Enumeration.METHOD_OF_PAYMENT methodOfPayment;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Column(name = "invoice_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date invoiceDate = new Date();
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(TemporalType.DATE)
    @Column(name = "due_date")
    private Date dueDate; // echaeance
    @Enumerated(EnumType.STRING)
    private Enumeration.INVOICE_STATE state = Enumeration.INVOICE_STATE.OPEN;
    @Column(name = "amount_paid")
    private Double amountPaid = 0D; // montant payer
    @Column(name = "stay_to_pay")
    private Double stayToPay = 0D; // balance = reste a payer
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_payment_date")
    private Date lastPaymentDate;
    @Column(name = "payment_count")
    private int paymentCount;
    private boolean paid = false;
    @Column(name = "editable")
    private boolean editable = true;
    @ManyToOne
    private Devis devis;

}
