/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities.parents;

import com.dsnmali.dev.dsnerpservice.entities.CashDesk;
import com.dsnmali.dev.dsnerpservice.entities.User;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author meididiallo
 */
@XmlRootElement
@MappedSuperclass
@DynamicUpdate
public abstract class CashDeskOperation extends AuditableParent<CashDeskOperation> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String number;
    private String designation;
    private String comment;
    private Long amount = 0L;
    @Column(name = "cash_balance_before")
    private long cashBalanceBefore;
    @Column(name = "cash_balance_after")
    private long cashBalanceAfter;
    @Enumerated(EnumType.STRING)
    private Enumeration.STATE state = Enumeration.STATE.VALID;
    private boolean synced;
    @ManyToOne
    @JoinColumn(name = "cash_desk_id")
    private CashDesk cashDesk;
    @ManyToOne
    @JoinColumn(name = "performed_id")
    private User performedBy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public long getCashBalanceBefore() {
        return cashBalanceBefore;
    }

    public void setCashBalanceBefore(long cashBalanceBefore) {
        this.cashBalanceBefore = cashBalanceBefore;
    }

    public long getCashBalanceAfter() {
        return cashBalanceAfter;
    }

    public void setCashBalanceAfter(long cashBalanceAfter) {
        this.cashBalanceAfter = cashBalanceAfter;
    }

    public Enumeration.STATE getState() {
        return state;
    }

    public void setState(Enumeration.STATE state) {
        this.state = state;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public CashDesk getCashDesk() {
        return cashDesk;
    }

    public void setCashDesk(CashDesk cashDesk) {
        this.cashDesk = cashDesk;
    }

    public User getPerformedBy() {
        return performedBy;
    }

    public void setPerformedBy(User performedBy) {
        this.performedBy = performedBy;
    }
}
