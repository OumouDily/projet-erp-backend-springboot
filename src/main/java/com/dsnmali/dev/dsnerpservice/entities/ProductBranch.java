/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "produit_secteur")
public class ProductBranch implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Product product;
    @ManyToOne
    private BranchActivity branchActivity;

    public ProductBranch() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductBranch)) {
            return false;
        }
        ProductBranch other = (ProductBranch) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "ProductBranch[ id=" + id + " ]";
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BranchActivity getBranchActivity() {
        return branchActivity;
    }

    public void setBranchActivity(BranchActivity branchActivity) {
        this.branchActivity = branchActivity;
    }

}
