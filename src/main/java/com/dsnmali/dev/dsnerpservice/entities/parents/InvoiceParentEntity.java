package com.dsnmali.dev.dsnerpservice.entities.parents;

import com.dsnmali.dev.dsnerpservice.entities.Customer;
import com.dsnmali.dev.dsnerpservice.entities.User;
import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * @author meididiallo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@XmlRootElement
@MappedSuperclass
@DynamicUpdate
public abstract class InvoiceParentEntity extends Auditable<InvoiceParentEntity> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String number;
    @Column(name = "invoice_address")
    private String invoiceAddress;
    @Column(name = "tax")
    private Integer taxValue;
    private String comment;
    private Double discount;
    @Column(name = "private_comment")
    private String privateComment;
    @Column(name = "amount_in_letter")
    private transient String amountInLetter;
    @Column(name = "sub_total")
    private Double subTotal;
    @Column(name = "total")
    private Double total;
    @Column(name = "total_discount")
    private Double totalDiscount;
    @Column(name = "tax_amount")
    private Double taxAmount;
    @Column(name = "enabled")
    private boolean enabled = true;
    @Column(name = "show_reglement_mode")
    private boolean showReglementMode = true;
    @Column(name = "show_comment")
    private boolean showComment = true;
    @ManyToOne
    private Customer customer;
    @ManyToOne
    private User agent;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cancel_date")
    private Date cancelDate;
    @ManyToOne
    @JoinColumn(name = "cancel_by")
    private User cancelBy;

}
