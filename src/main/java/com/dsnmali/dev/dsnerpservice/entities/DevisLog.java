package com.dsnmali.dev.dsnerpservice.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "devis_log")
@Data
@NoArgsConstructor
public class DevisLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String number;
    @Temporal(TemporalType.DATE)
    private Date date;
    @Column(name = "reason")
    private String reason;

    public DevisLog(String number, Date date, String reason) {
        this.number = number;
        this.date = date;
        this.reason = reason;
    }
}
