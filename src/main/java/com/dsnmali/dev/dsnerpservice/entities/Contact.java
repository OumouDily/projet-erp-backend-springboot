package com.dsnmali.dev.dsnerpservice.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "contact")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "phone_one")
    private String phoneOne;
    @Column(name = "phone_two")
    private String phoneTwo;
    @Column(name = "job")
    private String job;
    @Column(name = "email")
    private String email;
    @Column(name = "address")
    private String address;
    @Column(name = "notes")
    private String notes;
    @Column(name = "company")
    private String company;
    @Column(name = "country")
    private String country;
    private transient Set<Customer> customers;
}
