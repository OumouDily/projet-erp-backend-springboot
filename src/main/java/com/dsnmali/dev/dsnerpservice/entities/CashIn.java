/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.entities.parents.CashDeskOperation;
import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "cash_in")
public class CashIn extends CashDeskOperation implements Serializable, Comparable<CashIn> {


    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "cash_in_date")
    private Date cashInDate = new Date();

    public CashIn() {
    }

    public CashIn(Long id) {
        this.setId(id);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CashIn other = (CashIn) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    @Override
    public String toString() {
        return "CashIn{" + "id=" + this.getId() + '}';
    }

    @Override
    public int compareTo(CashIn o) {
        if (o == null) {
            return 1;
        }
        return cashInDate.compareTo(o.cashInDate);
    }

    public Date getCashInDate() {
        return cashInDate;
    }

    public void setCashInDate(Date cashInDate) {
        this.cashInDate = cashInDate;
    }

}
