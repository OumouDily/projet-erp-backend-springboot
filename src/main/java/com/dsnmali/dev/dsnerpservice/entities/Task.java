/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.entities.parents.Auditable;
import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "task")
public class Task extends Auditable<Task> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    private Enumeration.TASK_ACTION title;
    @Lob
    private String description;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate = new Date();
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "due_date")
    private Date dueDate = new Date();
    @Enumerated(EnumType.STRING)
    private Enumeration.TASK_STATE state;
    @Enumerated(EnumType.STRING)
    private Enumeration.TASK_PRIORITY priority;
    private boolean enabled = true;
    @ManyToOne
    @JoinColumn(name = "assign_to")
    private User assignTo;
    @ManyToOne
    @JoinColumn(name = "prospect_client_id")
    private Customer customer;
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public Task() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Task)) {
            return false;
        }
        Task other = (Task) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "Task[ id=" + id + " ]";
    }

    public Enumeration.TASK_ACTION getTitle() {
        return title;
    }

    public void setTitle(Enumeration.TASK_ACTION title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Enumeration.TASK_STATE getState() {
        return state;
    }

    public void setState(Enumeration.TASK_STATE state) {
        this.state = state;
    }

    public Enumeration.TASK_PRIORITY getPriority() {
        return priority;
    }

    public void setPriority(Enumeration.TASK_PRIORITY priority) {
        this.priority = priority;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public User getAssignTo() {
        return assignTo;
    }

    public void setAssignTo(User assignTo) {
        this.assignTo = assignTo;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
