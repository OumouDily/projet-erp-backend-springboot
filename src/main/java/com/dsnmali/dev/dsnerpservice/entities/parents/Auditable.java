/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities.parents;

import com.dsnmali.dev.dsnerpservice.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author meididiallo
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@XmlRootElement
@MappedSuperclass
public abstract class Auditable<T> extends AuditableParent<Auditable> implements Serializable {

    @ManyToOne
    @JoinColumn(name = "create_by")
    private User createBy;
    @ManyToOne
    @JoinColumn(name = "update_by")
    private User updateBy;

}
