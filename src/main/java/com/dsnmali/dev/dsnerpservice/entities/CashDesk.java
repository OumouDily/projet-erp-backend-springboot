/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.entities.parents.AuditableParent;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "cash_desk")
public class CashDesk extends AuditableParent<CashDesk> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long balance = 0L;
    @Enumerated(EnumType.STRING)
    private Enumeration.CASH_DESK_TYPE type = Enumeration.CASH_DESK_TYPE.PROCUREMENT;
    private boolean enabled = true;
    private boolean synced = false;
    @ManyToOne
    @JoinColumn(name = "in_charge")
    private User inCharge;

    public CashDesk() {
    }

    public CashDesk(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CashDesk other = (CashDesk) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "CashDesk{" + "id=" + id + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Enumeration.CASH_DESK_TYPE getType() {
        return type;
    }

    public void setType(Enumeration.CASH_DESK_TYPE type) {
        this.type = type;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public User getInCharge() {
        return inCharge;
    }

    public void setInCharge(User inCharge) {
        this.inCharge = inCharge;
    }
}
