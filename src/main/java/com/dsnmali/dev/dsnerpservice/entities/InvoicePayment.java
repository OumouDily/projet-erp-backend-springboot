/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "invoice_payment")
public class
InvoicePayment implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String number;
    private String reference;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "payment_date")
    private Date paymentDate = new Date();
    @Column(name = "method_of_payment")
    @Enumerated(EnumType.STRING)
    private Enumeration.METHOD_OF_PAYMENT methodOfPayment;
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private Enumeration.PAYMENT_STATE state;
    private Double amount;
    @Column(name = "net_to_pay")
    private Double netToPay;
    @Column(name = "balance_before")
    private Double balanceBefore;
    @Column(name = "balance_after")
    private Double balanceAfter;
    @Column(name = "account_paid_before")
    private Double accountPaidBefore = 0D; // acount verser
    @Column(name = "account_paid_after")
    private Double accountPaidAfter = 0D;
    @Column(name = "amount_in_letter")
    private transient String amountInLetter;
    private boolean canceled = false;
    @ManyToOne
    @JoinColumn(name = "invoice_id")
    private Invoice invoice;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    private Date createDate;
    @ManyToOne
    @JoinColumn(name = "create_by")
    private User createBy;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "validate_date")
    private Date validateDate;
    @ManyToOne
    @JoinColumn(name = "validate_by")
    private User validateBy;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cancel_date")
    private Date cancelDate;
    @ManyToOne
    @JoinColumn(name = "cancel_by")
    private User cancelBy;

    public InvoicePayment() {
    }

    public InvoicePayment(InvoicePayment invoicePayment) {
        this.setNumber(invoicePayment.getNumber());
        this.setBalanceBefore(invoicePayment.getBalanceBefore());
        this.setBalanceAfter(invoicePayment.getBalanceAfter());
        this.setAmount(invoicePayment.getAmount());
        this.setNetToPay(invoicePayment.getNetToPay());
        this.setPaymentDate(invoicePayment.getPaymentDate());
        this.setCreateBy(invoicePayment.getCreateBy());
        this.setValidateBy(invoicePayment.getValidateBy());
        this.setReference(invoicePayment.getReference());
        this.setMethodOfPayment(invoicePayment.getMethodOfPayment());
        this.setInvoice(invoicePayment.getInvoice());
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvoicePayment)) {
            return false;
        }
        InvoicePayment other = (InvoicePayment) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "InvoicePayment[ id=" + id + " ]";
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Enumeration.METHOD_OF_PAYMENT getMethodOfPayment() {
        return methodOfPayment;
    }

    public void setMethodOfPayment(Enumeration.METHOD_OF_PAYMENT methodOfPayment) {
        this.methodOfPayment = methodOfPayment;
    }

    public Enumeration.PAYMENT_STATE getState() {
        return state;
    }

    public void setState(Enumeration.PAYMENT_STATE state) {
        this.state = state;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getNetToPay() {
        return netToPay;
    }

    public void setNetToPay(Double netToPay) {
        this.netToPay = netToPay;
    }

    public Double getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(Double balanceBefore) {
        this.balanceBefore = balanceBefore;
    }

    public Double getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(Double balanceAfter) {
        this.balanceAfter = balanceAfter;
    }

    public Double getAccountPaidBefore() {
        return accountPaidBefore;
    }

    public void setAccountPaidBefore(Double amountPaid) {
        this.accountPaidBefore = amountPaid;
    }

    public Double getAccountPaidAfter() {
        return accountPaidAfter;
    }

    public void setAccountPaidAfter(Double accountPaidAfter) {
        this.accountPaidAfter = accountPaidAfter;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public String getAmountInLetter() {
        return amountInLetter;
    }

    public void setAmountInLetter(String amountInLetter) {
        this.amountInLetter = amountInLetter;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public Date getValidateDate() {
        return validateDate;
    }

    public void setValidateDate(Date validateDate) {
        this.validateDate = validateDate;
    }

    public User getValidateBy() {
        return validateBy;
    }

    public void setValidateBy(User validateBy) {
        this.validateBy = validateBy;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public User getCancelBy() {
        return cancelBy;
    }

    public void setCancelBy(User cancelBy) {
        this.cancelBy = cancelBy;
    }
}
