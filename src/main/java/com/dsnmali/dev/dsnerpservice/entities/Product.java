/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.entities.parents.AuditableParent;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "product")
public class Product extends AuditableParent<Product> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String solution;
    private String reference;
    private String designation;
    private Long quantity;
    @Column(name = "unit_price_euro")
    private Long unitPriceEURO;
    @Column(name = "unit_price_xof")
    private Long unitPriceXOF;
    @Column(name = "distributive_price")
    private Long distributivePrice;
    @Column(name = "customer_price")
    private Long customerPrice;
    private boolean enabled = true;
    @ManyToOne
    @JoinColumn(name = "product_category_id")
    private ProductCategory productCategory;
    @ManyToOne
    private Provider provider;
    @ManyToOne
    @JoinColumn(name = "product_nature_id")
    private ProductNature productNature;
    private transient List<BranchActivity> branchActivities;

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Product)) {
            return false;
        }
        Product other = (Product) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "Product[ id=" + id + " ]";
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getUnitPriceEURO() {
        return unitPriceEURO;
    }

    public void setUnitPriceEURO(Long unitPriceEURO) {
        this.unitPriceEURO = unitPriceEURO;
    }

    public Long getUnitPriceXOF() {
        return unitPriceXOF;
    }

    public void setUnitPriceXOF(Long unitPriceXOF) {
        this.unitPriceXOF = unitPriceXOF;
    }

    public Long getDistributivePrice() {
        return distributivePrice;
    }

    public void setDistributivePrice(Long distributivePrice) {
        this.distributivePrice = distributivePrice;
    }

    public Long getCustomerPrice() {
        return customerPrice;
    }

    public void setCustomerPrice(Long customerPrice) {
        this.customerPrice = customerPrice;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory categorie) {
        this.productCategory = categorie;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public ProductNature getProductNature() {
        return productNature;
    }

    public void setProductNature(ProductNature productNature) {
        this.productNature = productNature;
    }

    public List<BranchActivity> getBranchActivities() {
        return branchActivities;
    }

    public void setBranchActivities(List<BranchActivity> branchActivities) {
        this.branchActivities = branchActivities;
    }
}
