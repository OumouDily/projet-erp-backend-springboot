/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "dish_order_line")
public class DishOrderLine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long quantity = 0L;
    private Long price = 0L;
    private Long amount = 0L;
    @ManyToOne
    @JoinColumn(name = "dish_order_id")
    private DishOrder dishOrder;
    @ManyToOne
    private Dish dish;

    public DishOrderLine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DishOrderLine)) {
            return false;
        }
        DishOrderLine other = (DishOrderLine) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "DishOrderLine{" + "id=" + id + ", quantity=" + quantity + ", price=" + price + ", amount=" + amount + ", dishOrder=" + dishOrder + ", dish=" + dish + '}';
    }


    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long prix) {
        this.price = prix;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long montant) {
        this.amount = montant;
    }

    public DishOrder getDishOrder() {
        return dishOrder;
    }

    public void setDishOrder(DishOrder dishOrder) {
        this.dishOrder = dishOrder;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }


}
