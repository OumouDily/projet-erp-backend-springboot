/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;


import com.dsnmali.dev.dsnerpservice.entities.parents.AuditableParent;
import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "cash_out_details")
public class CashOutDetails extends AuditableParent<CashOutDetails> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    private long amount;
    @Column(name = "balance_before")
    private long balanceBefore;
    @Column(name = "balance_after")
    private long balanceAfter;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "details_date")
    private Date detailsDate = new Date();
    private String designation;
    @OneToOne
    private CashOut cashOut;
    @ManyToOne
    @JoinColumn(name = "staff_id")
    private User staff;
    @Enumerated(EnumType.STRING)
    private Enumeration.STATE state = Enumeration.STATE.VALID;
    private boolean synced;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CashOutDetails)) {
            return false;
        }
        CashOutDetails other = (CashOutDetails) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "CashOutDetails{" + "id=" + id + '}';
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long montantDetail) {
        this.amount = montantDetail;
    }

    public long getBalanceBefore() {
        return balanceBefore;
    }

    public void setBalanceBefore(long balanceSortieAvant) {
        this.balanceBefore = balanceSortieAvant;
    }

    public long getBalanceAfter() {
        return balanceAfter;
    }

    public void setBalanceAfter(long balanceSortieApres) {
        this.balanceAfter = balanceSortieApres;
    }

    public Date getDetailsDate() {
        return detailsDate;
    }

    public void setDetailsDate(Date dateDetail) {
        this.detailsDate = dateDetail;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public CashOut getCashOut() {
        return cashOut;
    }

    public void setCashOut(CashOut cashOut) {
        this.cashOut = cashOut;
    }

    public User getStaff() {
        return staff;
    }

    public void setStaff(User personnel) {
        this.staff = personnel;
    }

    public Enumeration.STATE getState() {
        return state;
    }

    public void setState(Enumeration.STATE STATE) {
        this.state = state;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

}
