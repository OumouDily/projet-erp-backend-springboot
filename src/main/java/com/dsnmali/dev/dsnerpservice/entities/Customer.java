/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.entities.parents.Auditable;
import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "customer")
public class Customer extends Auditable<Customer> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(name = "judicial_nature", length = 10)
    private String judicialNature;
    @Column(name = "phone_one")
    private String telephoneOne;
    @Column(name = "phone_two")
    private String telephoneTwo;
    private String email;
    @Column(name = "address_one")
    private String addressOne;
    @Column(name = "address_two")
    private String addressTwo;
    @Column(name = "web_site")
    private String siteWeb;
    private Long turnover;
    @Column(name = "number_of_employees")
    private Long numberOfEmployees;
    @Column(name = "contact_name")
    private String contactName;
    @Column(name = "contact_phone")
    private String telephoneContact;
    @Lob
    private String description;
    @Enumerated(EnumType.STRING)
    private Enumeration.CUSTOMER_STATE state = Enumeration.CUSTOMER_STATE.NOT_CONTACTED;
    @Enumerated(EnumType.STRING)
    private Enumeration.CUSTOMER_TYPE type = Enumeration.CUSTOMER_TYPE.PROSPECT;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "transformation_date")
    private Date transformationDate;
    private boolean enabled = true;
    @ManyToOne
    @JoinColumn(name = "agent_id")
    private User agent;
    @ManyToOne
    @JoinColumn(name = "branch_activity_id")
    private BranchActivity branchActivity;


    private transient Set<Contact> contacts;

    public Customer() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "Customer[ id=" + id + " ]";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJudicialNature() {
        return judicialNature;
    }

    public void setJudicialNature(String judicialNature) {
        this.judicialNature = judicialNature;
    }

    public String getTelephoneOne() {
        return telephoneOne;
    }

    public void setTelephoneOne(String telephoneOne) {
        this.telephoneOne = telephoneOne;
    }

    public String getTelephoneTwo() {
        return telephoneTwo;
    }

    public void setTelephoneTwo(String telephoneTwo) {
        this.telephoneTwo = telephoneTwo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddressOne() {
        return addressOne;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public String getAddressTwo() {
        return addressTwo;
    }

    public void setAddressTwo(String addressTwo) {
        this.addressTwo = addressTwo;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public Long getTurnover() {
        return turnover;
    }

    public void setTurnover(Long turnover) {
        this.turnover = turnover;
    }

    public Long getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public void setNumberOfEmployees(Long numberOfEmployees) {
        this.numberOfEmployees = numberOfEmployees;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getTelephoneContact() {
        return telephoneContact;
    }

    public void setTelephoneContact(String telephoneContact) {
        this.telephoneContact = telephoneContact;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Enumeration.CUSTOMER_STATE getState() {
        return state;
    }

    public void setState(Enumeration.CUSTOMER_STATE state) {
        this.state = state;
    }

    public Enumeration.CUSTOMER_TYPE getType() {
        return type;
    }

    public void setType(Enumeration.CUSTOMER_TYPE type) {
        this.type = type;
    }

    public Date getTransformationDate() {
        return transformationDate;
    }

    public void setTransformationDate(Date transformationDate) {
        this.transformationDate = transformationDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public User getAgent() {
        return agent;
    }

    public void setAgent(User agent) {
        this.agent = agent;
    }

    public BranchActivity getBranchActivity() {
        return branchActivity;
    }

    public void setBranchActivity(BranchActivity branchActivity) {
        this.branchActivity = branchActivity;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }
}
