/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 * @author meididiallo
 */
@Entity
@XmlRootElement
@Table(name = "log")
public class Log implements Serializable, Comparable<Log> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "log_action")
    private String logAction;
    @ManyToOne
    @JoinColumn(name = "log_author")
    private User logAuthor;
    private String description;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "log_date")
    private Date logDate;

    public Log() {
    }

    public Log(String logAction, User acteur, String description) {
        this.logAction = logAction;
        this.logAuthor = acteur;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Log)) {
            return false;
        }
        Log other = (Log) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "Log[ id=" + id + " ]";
    }

    @Override
    public int compareTo(Log o) {
        if (o == null) {
            return 1;
        }
        return logDate.compareTo(o.logDate);
    }

    public String getLogAction() {
        return logAction;
    }

    public void setLogAction(String logAction) {
        this.logAction = logAction;
    }

    public User getLogAuthor() {
        return logAuthor;
    }

    public void setLogAuthor(User logAuthor) {
        this.logAuthor = logAuthor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

}
