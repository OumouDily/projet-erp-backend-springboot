package com.dsnmali.dev.dsnerpservice.entities;


import com.dsnmali.dev.dsnerpservice.entities.parents.Auditable;
import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "project_folder")
public class ProjectFolder extends Auditable<ProjectFolder> implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Enumerated(EnumType.STRING)
    private Enumeration.PROJECT_FOLDER_PRIORITY priority;
    @Column(name = "chance_rate")
    private int chanceRate;
    @Enumerated(EnumType.STRING)
    private Enumeration.PROJECT_FOLDER_STATE state;
    @Column(name = "start_date")
    private Date startDate = new Date();
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;
    @Column(name = "client_budget")
    private Double clientBudget;
    @Column(name = "turnover")
    private Double turnover;
    @Column(name = "note")
    private String note;
    @ManyToOne
    @JoinColumn(name = "agent_id")
    private User agent;
    @ManyToOne
    @JoinColumn(name = "customer")
    private Customer customer;


    public ProjectFolder() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Enumeration.PROJECT_FOLDER_PRIORITY getPriority() {
        return priority;
    }

    public Double getClientBudget() {
        return clientBudget;
    }

    public void setClientBudget(Double clientBudget) {
        this.clientBudget = clientBudget;
    }

    public Double getTurnover() {
        return turnover;
    }

    public void setTurnover(Double CA) {
        this.turnover = CA;
    }

    public void setPriority(Enumeration.PROJECT_FOLDER_PRIORITY priority) {
        this.priority = priority;
    }

    public int getChanceRate() {
        return chanceRate;
    }

    public void setChanceRate(int chanceRate) {
        this.chanceRate = chanceRate;
    }

    public Enumeration.PROJECT_FOLDER_STATE getState() {
        return state;
    }

    public void setState(Enumeration.PROJECT_FOLDER_STATE state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public User getAgent() {
        return agent;
    }

    public void setAgent(User agent) {
        this.agent = agent;
    }
}

