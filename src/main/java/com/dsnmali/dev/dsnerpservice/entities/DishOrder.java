/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.entities.parents.Auditable;
import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "dish_order")
public class DishOrder extends Auditable<DishOrder> implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "dish_quantity")
    private Long dishQuantity;
    private Long amount;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "order_date")
    private Date orderDate = new Date();
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "cancel_date")
    private Date cancelDate;
    @Enumerated(EnumType.STRING)
    private Enumeration.STATE state = Enumeration.STATE.PENDING;
    private boolean enabled = true;
    private boolean canceled = false;
    @ManyToOne
    @JoinColumn(name = "staff_id")
    private User staff;
    @ManyToOne
    @JoinColumn(name = "cancel_by")
    private User cancelBy;
    private transient List<DishOrderLine> dishOrderLines;

    public DishOrder() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DishOrder)) {
            return false;
        }
        DishOrder other = (DishOrder) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "Commande[ id=" + id + " ]";
    }

    public Long getDishQuantity() {
        return dishQuantity;
    }

    public void setDishQuantity(Long dishQuantity) {
        this.dishQuantity = dishQuantity;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long montant) {
        this.amount = montant;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date dateCommande) {
        this.orderDate = dateCommande;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public User getCancelBy() {
        return cancelBy;
    }

    public void setCancelBy(User cancelBy) {
        this.cancelBy = cancelBy;
    }

    public Enumeration.STATE getState() {
        return state;
    }

    public void setState(Enumeration.STATE STATE) {
        this.state = state;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public User getStaff() {
        return staff;
    }

    public void setStaff(User personnel) {
        this.staff = personnel;
    }

    public List<DishOrderLine> getDishOrderLines() {
        return dishOrderLines;
    }

    public void setDishOrderLines(List<DishOrderLine> dishOrderLines) {
        this.dishOrderLines = dishOrderLines;
    }

}
