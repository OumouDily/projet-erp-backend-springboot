package com.dsnmali.dev.dsnerpservice.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "customer_contact")
public class CustomerContact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "contact_id")
    private Contact contact;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public CustomerContact(Contact contact, Customer customer) {
        this.contact = contact;
        this.customer = customer;
    }
}
