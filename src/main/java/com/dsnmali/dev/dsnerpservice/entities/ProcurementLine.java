/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "procurement_line")
public class ProcurementLine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String designation;
    private Long quantity = 0L;
    @Column(name = "unit_price")
    private Long unitPrice = 0L;
    private Long amount = 0L; // montant Hors Tax
    @ManyToOne
    private Procurement procurement;

    public ProcurementLine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProcurementLine)) {
            return false;
        }
        ProcurementLine other = (ProcurementLine) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "ProcurementLine[ id=" + id + " ]";
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Long prixUnitaire) {
        this.unitPrice = prixUnitaire;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long montant) {
        this.amount = montant;
    }

    public Procurement getProcurement() {
        return procurement;
    }

    public void setProcurement(Procurement procurement) {
        this.procurement = procurement;
    }

}
