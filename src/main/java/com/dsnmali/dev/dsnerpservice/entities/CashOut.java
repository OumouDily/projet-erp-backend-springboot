/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.entities.parents.CashDeskOperation;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author meididiallo
 */
@Entity
@Table(name = "cash_out")
public class CashOut extends CashDeskOperation implements Serializable, Comparable<CashOut> {

    private Date cashOutDate = new Date();

    public CashOut() {
    }

    public CashOut(Long id) {
        this.setId(id);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CashOut other = (CashOut) obj;
        return Objects.equals(this.getId(), other.getId());
    }

    @Override
    public int compareTo(CashOut o) {
        if (o == null) {
            return 1;
        }
        return cashOutDate.compareTo(o.cashOutDate);
    }

    @Override
    public String toString() {
        return "CashOut{" + "id=" + this.getId() + '}';
    }


    public Date getCashOutDate() {
        return cashOutDate;
    }

    public void setCashOutDate(Date cashOutDate) {
        this.cashOutDate = cashOutDate;
    }
}
