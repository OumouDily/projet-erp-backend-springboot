package com.dsnmali.dev.dsnerpservice.entities;

import com.dsnmali.dev.dsnerpservice.entities.parents.InvoiceParentEntity;
import com.dsnmali.dev.dsnerpservice.utils.CustomJsonDateUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "devis")
public class Devis extends InvoiceParentEntity {

    @Column(name = "amount_invoiced")
    private Double amountInvoiced;
    @JsonSerialize(using = CustomJsonDateUtils.JsonDateSerializer.class)
    @Column(name = "devis_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date devisDate = new Date();
    @Enumerated(EnumType.STRING)
    private Enumeration.DEVIS_STATE state = Enumeration.DEVIS_STATE.OPEN;
    @Column(name = "invoice_generated")
    private boolean invoiceGenerated = false;
    @Column(name = "can_duplicate")
    private boolean canDuplicate = false;


}
