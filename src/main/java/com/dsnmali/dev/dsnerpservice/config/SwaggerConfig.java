/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.Optional;

/**
 * @author meididiallo
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)//
                .select()//
                .apis(RequestHandlerSelectors.basePackage("com.dsnmali.dev.dsnerpservice.controllers"))
                .paths(Predicates.not(PathSelectors.regex("/error")))//
                .build()//
                .apiInfo(apiInfo())//
                .useDefaultResponseMessages(false)//
                .securitySchemes(Collections.singletonList(new ApiKey("Bearer %token", "Authorization", "header")))//
                .genericModelSubstitutes(Optional.class);

    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("DSNERP REST API")
                .description("DSN MALI REST API")
                .contact(new Contact("DSN MALI SARL", "www.dsnmali.com", "dsnapps@dsnmali.com"))
                .license("Dsn Mali 1.0")
                .version("1.0.0")
                .build();
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
