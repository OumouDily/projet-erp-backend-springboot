package com.dsnmali.dev.dsnerpservice.components;

import com.dsnmali.dev.dsnerpservice.entities.Parameter;
import com.dsnmali.dev.dsnerpservice.respositories.ParameterRepository;
import com.dsnmali.dev.dsnerpservice.utils.DsnUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Date;

@Component
public class ParameterComponent {

    private final ParameterRepository parameterRepository;

    public ParameterComponent(ParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    public String generateInvoiceNumber(Enumeration.INVOICE_TYPE invoiceType) {
        try {
            Parameter params = parameterRepository.findParameterById(1L);
            if (invoiceType == Enumeration.INVOICE_TYPE.DEVIS) {
                // return invoiceType.getDesc() + "-" + DsnUtils.dateToString(new Date(), "MMyyyy") + "-" + DsnUtils.generateStringNumber(params.getLastDevisNumber() + 1);
                return DsnUtils.toString(new Date(), "MMyyyy") + "-" + DsnUtils.generateStringNumber(params.getLastDevisNumber() + 1);
            } else if (invoiceType == Enumeration.INVOICE_TYPE.FACT) {
                // return invoiceType.getDesc() + "-" + DsnUtils.dateToString(new Date(), "MMyyyy") + "-" + DsnUtils.generateStringNumber(params.getLastInvoiceNumber() + 1);
                return DsnUtils.toString(new Date(), "MMyyyy") + "-" + DsnUtils.generateStringNumber(params.getLastInvoiceNumber() + 1);
            }
        } catch (ParseException e) {
            e.printStackTrace(System.err);
        }
        return "";
    }

    public void updateInvoiceNumber(Enumeration.INVOICE_TYPE invoiceType) {
        Parameter params = parameterRepository.findParameterById(1L);
        if (invoiceType == Enumeration.INVOICE_TYPE.DEVIS) {
            params.setLastDevisNumber(params.getLastDevisNumber() + 1);
        } else if (invoiceType == Enumeration.INVOICE_TYPE.FACT) {
            params.setLastInvoiceNumber(params.getLastInvoiceNumber() + 1);
        }
        parameterRepository.save(params);
    }
}
