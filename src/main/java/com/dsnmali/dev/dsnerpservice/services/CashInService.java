package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.CashInRepository;
import org.springframework.stereotype.Service;

@Service
public class CashInService {

    final CashInRepository checkInRepository;

    public CashInService(CashInRepository checkInRepository) {
        this.checkInRepository = checkInRepository;
    }
}
