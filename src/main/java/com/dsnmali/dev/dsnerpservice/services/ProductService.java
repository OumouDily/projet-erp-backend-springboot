package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.Product;
import com.dsnmali.dev.dsnerpservice.respositories.ProductRepository;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProductService {

    private final
    ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ResponseBody getById(Long id) {
        try {
            Optional<Product> optionalProduct = productRepository.findById(id);
            if (!optionalProduct.isPresent()) {
                return ResponseBody.error("Ce produit / service n'existe pas");
            }
            Product product = optionalProduct.get();
            return ResponseBody.with(product, "Produit / Service recupere avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody save(Product product) {
        try {
            return ResponseBody.with(productRepository.save(product), "Produit / Service ajoute avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody edit(Product product) {
        try {
            return ResponseBody.with(productRepository.save(product), "Produit / Service modifie avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody delete(Long id) {
        try {
            productRepository.deleteById(id);
            return ResponseBody.success("Produit / Service supprime avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody getAll(Long categoryId) {
        try {
            if (categoryId != null && categoryId != 0) {
                Set<Product> products = productRepository.findAllByProductCategoryId(categoryId);
                return ResponseBody.with(products, products.size() + " produit..s trouve.s");
            }
            Set<Product> products = productRepository.findAllProducts();
            return ResponseBody.with(products, products.size() + " produit.s trouves");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }

}
