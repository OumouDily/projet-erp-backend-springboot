package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.CashOutRepository;
import org.springframework.stereotype.Service;

@Service
public class CashOutService {
    private final
    CashOutRepository cashOutRepository;

    public CashOutService(CashOutRepository cashOutRepository) {
        this.cashOutRepository = cashOutRepository;
    }
}
