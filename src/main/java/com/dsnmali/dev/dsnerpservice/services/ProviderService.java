package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.ProviderRepository;
import org.springframework.stereotype.Service;

@Service
public class ProviderService {

    final ProviderRepository providerRepository;

    public ProviderService(ProviderRepository providerRepository) {
        this.providerRepository = providerRepository;
    }
}
