package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.DevisLine;
import com.dsnmali.dev.dsnerpservice.respositories.DevisLineRepository;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DevisLineService {

    private final DevisLineRepository devisLineRepository;

    public DevisLineService(DevisLineRepository devisLineRepository) {
        this.devisLineRepository = devisLineRepository;
    }

    public ResponseBody create(DevisLine devisLine) {
        System.out.println("Ligne devis : " + devisLine.toString());
        try {
            return ResponseBody.with(devisLineRepository.save(devisLine), "Ajoute avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public List<DevisLine> findListByDevis(Long id) {
        return devisLineRepository.findByDevis(id);
    }

    public ResponseBody findDevisLine(Long id) {
        try {
            return ResponseBody.with(devisLineRepository.findDevisLine(id), "Recuperer avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public void deleteAllDevisLine(List<DevisLine> devisLines) {
        devisLineRepository.deleteAll(devisLines);
    }
}
