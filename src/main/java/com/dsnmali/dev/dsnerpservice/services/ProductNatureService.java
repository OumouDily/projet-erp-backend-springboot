package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.ProductNatureRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductNatureService {

    final ProductNatureRepository productNatureRepository;

    public ProductNatureService(ProductNatureRepository productNatureRepository) {
        this.productNatureRepository = productNatureRepository;
    }
}
