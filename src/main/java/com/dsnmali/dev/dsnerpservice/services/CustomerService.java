package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.Contact;
import com.dsnmali.dev.dsnerpservice.entities.Customer;
import com.dsnmali.dev.dsnerpservice.entities.CustomerContact;
import com.dsnmali.dev.dsnerpservice.respositories.ContactRepository;
import com.dsnmali.dev.dsnerpservice.respositories.CustomerContactRepository;
import com.dsnmali.dev.dsnerpservice.respositories.CustomerRepository;
import com.dsnmali.dev.dsnerpservice.respositories.InvoiceRepository;
import com.dsnmali.dev.dsnerpservice.utils.DsnUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import com.dsnmali.dev.dsnerpservice.utils.dashboard.CrmDashBody;
import com.dsnmali.dev.dsnerpservice.utils.dashboard.GraphBody;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

import static java.util.stream.Collectors.toMap;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerContactRepository customerContactRepository;
    private final ContactRepository contactRepository;
    private final InvoiceRepository invoiceRepository;

    public CustomerService(CustomerRepository customerRepository, CustomerContactRepository customerContactRepository,
                           ContactRepository contactRepository, InvoiceRepository invoiceRepository) {
        this.customerRepository = customerRepository;
        this.customerContactRepository = customerContactRepository;
        this.contactRepository = contactRepository;
        this.invoiceRepository = invoiceRepository;
    }

    public Page<Customer> getAllProspect(SearchBody searchBody) {
        Pageable pageable = PageRequest.of(searchBody.getPage().orElse(0), 30,
                Sort.Direction.ASC, StringUtils.isEmpty(searchBody.getSortBy()) ? "name" : searchBody.getSortBy().toString());
        if (searchBody.getUserId() != 0) {
            return customerRepository.getCustomers(pageable, searchBody.getUserId(), Enumeration.CUSTOMER_TYPE.PROSPECT);
        } else {
            return customerRepository.getCustomers(pageable, Enumeration.CUSTOMER_TYPE.PROSPECT);
        }
    }

    public Page<Customer> getAllCustomer(SearchBody searchBody) {
        Pageable pageable = PageRequest.of(searchBody.getPage().orElse(0), 30,
                Sort.Direction.ASC, StringUtils.isEmpty(searchBody.getSortBy()) ? "name" : searchBody.getSortBy().toString());
        if (searchBody.getUserId() != 0) {
            return customerRepository.getCustomers(pageable, searchBody.getUserId(), Enumeration.CUSTOMER_TYPE.CLIENT);
        } else {
            return customerRepository.getCustomers(pageable, Enumeration.CUSTOMER_TYPE.CLIENT);
        }
    }

    public ResponseBody findAllClient(SearchBody searchBody) {
        try {
            List<Customer> clients;
            if (searchBody.getUserId() != 0) {
                clients = customerRepository.getCustomers(searchBody.getUserId(), Enumeration.CUSTOMER_TYPE.CLIENT);
            }
            else {
                clients = customerRepository.getCustomers(Enumeration.CUSTOMER_TYPE.CLIENT);
            }
            return ResponseBody.with(clients, clients.size() + " clients");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }

    public ResponseBody findAllProspect(SearchBody searchBody) {
        try {
            List<Customer> prospects;
            if (searchBody.getUserId() != 0) {
                prospects = customerRepository.getCustomers(searchBody.getUserId(), Enumeration.CUSTOMER_TYPE.PROSPECT);
            } else {
                prospects = customerRepository.getCustomers(Enumeration.CUSTOMER_TYPE.PROSPECT);
            }
            return ResponseBody.with(prospects, prospects.size() + " prospects");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }

    public ResponseBody findAll() {
        try {
            List<Customer> customers = customerRepository.findAll();
            return ResponseBody.with(customers, customers.size() + " prospect.s ou client.s trouve.s");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }

    public ResponseBody findProspectCustomer(SearchBody searchBody) {
        try {
            List<Customer> clients = new ArrayList<>();
            if (searchBody.getBranchId() == 0 && searchBody.getCreateById() != 0) {
                clients = customerRepository.findAllProspectClientByAgentId(searchBody.getCreateById(), searchBody.getTypeClient());
                return ResponseBody.with(clients, " Il y'a " + clients.size() + searchBody.getTypeClient());
            } else if (searchBody.getBranchId() != 0 && searchBody.getCreateById() == 0) {
                clients = customerRepository.findAllProspectClientBySector(searchBody.getBranchId(), searchBody.getTypeClient());
                return ResponseBody.with(clients, " Il y'a " + clients.size() + searchBody.getTypeClient());
            } else if (searchBody.getBranchId() != 0 && searchBody.getCreateById() != 0) {
                clients = customerRepository.findAllProspectBySectorAndAgentId(searchBody.getBranchId(), searchBody.getCreateById(), searchBody.getTypeClient());
                return ResponseBody.with(clients, " Il y'a " + clients.size() + searchBody.getTypeClient());
            } else if (searchBody.getContactId() != 0) {
                Set<Customer> customers = customerRepository.findAllByContactId(searchBody.getContactId());
                return ResponseBody.with(customers, " Il y'a " + customers.size() + " prospect.s ou client.s");
            } else if (searchBody.getBranchId() == 0 && searchBody.getCreateById() == 0) {
                clients = customerRepository.findAllProspectClientByType(searchBody.getTypeClient());
                return ResponseBody.with(clients, " Il y'a " + clients.size() + " prospect.s ou client.s");
            } else {
                clients = customerRepository.findAll();
                return ResponseBody.with(clients, clients.size() + " prospect.s ou client.s trouve.s");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }

    public Optional<Customer> findById(Long id) {
        return customerRepository.findById(id);
    }

    public ResponseBody getById(Long id) {
        try {
            Optional<Customer> optionalCustomer = customerRepository.findById(id);
            if (!optionalCustomer.isPresent()) {
                return ResponseBody.error("Ce client ou prospect n'existe pas");
            }
            Customer customer = optionalCustomer.get();
            Set<Contact> contacts = contactRepository.findAllByCustomerId(customer.getId());
            customer.setContacts(contacts);

            return ResponseBody.with(customerRepository.findById(id), "Recuperer avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody save(Customer customer) {
        try {
            if (customerRepository.existsByName(customer.getName())) {
                return ResponseBody.error("Ce nom existe deja!");
            }
            customerRepository.save(customer);
            if (customer.getContacts() != null) {
                customer.getContacts().forEach(contact -> customerContactRepository.save(new CustomerContact(contact, customer)));
            }
            return ResponseBody.with(customer, "Ajoute avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody edit(Customer customer) {
        try {
            // if empty so => not exist, else => exist
            boolean isExist = customerRepository.distinctByIdAndExistByName(customer.getId(), customer.getName()).isEmpty();
            if (!isExist) {
                return ResponseBody.error("Ce nom existe deja!");
            }
            customerRepository.save(customer);

            Set<CustomerContact> customerContacts = customerContactRepository.findAllByCustomerId(customer.getId());
            customerContacts.forEach(customerContactRepository::delete);

            if (customer.getContacts() != null) {
                customer.getContacts().forEach(contact -> {
                    customerContactRepository.save(new CustomerContact(contact, customer));
                });
            }

            return ResponseBody.with(customer, "Modifie avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody deleteById(Long id) {
        try {
            if (customerRepository.findById(id).isPresent()) {
                customerRepository.deleteById(id);
                return ResponseBody.success("Suppression avec succes");
            }
            return ResponseBody.error("Ce client ou prospect n'existe pas");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue");
        }
    }

    public ResponseBody initCrmDash(SearchBody searchBody) {

        try {

            HashMap<String, Double> customerAmountGraph = new HashMap<>();

            int totalClientCount = 0;
            int totalProspectCount = 0;
            int newProspectCount = 0;
            int notContactedProspectCount = 0;
            Date startMonth = DsnUtils.subMonthToDate(new Date(), 1);

            List<Customer> customerList;
            if(searchBody.getCreateById()!=0) {
                customerList = customerRepository.getCustomers(searchBody.getCreateById());
            } else {
                customerList = customerRepository.findAll();
            }

            for (Customer c : customerList) {
                if (c.getType() == Enumeration.CUSTOMER_TYPE.PROSPECT) {
                    if (c.getState() == Enumeration.CUSTOMER_STATE.NOT_CONTACTED) {
                        notContactedProspectCount++;
                    }
                    if (c.getCreateDate() != null && c.getCreateDate().after(startMonth)) {
                        newProspectCount++;
                    }
                    totalProspectCount++;
                } else if (c.getType() == Enumeration.CUSTOMER_TYPE.CLIENT) {
                    totalClientCount++;
                }

                Double sumAmount = invoiceRepository.sumTotalByCustomerIdAndStateNot(c.getId(), Enumeration.INVOICE_STATE.CANCEL);
                customerAmountGraph.put(c.getName(), sumAmount == null ? 0 : sumAmount);
            }

            CrmDashBody crmDashBody = new CrmDashBody();
            crmDashBody.setNewProspectCount(newProspectCount);
            crmDashBody.setNotContactedProspectCount(notContactedProspectCount);
            crmDashBody.setCustomerCount(totalClientCount);
            crmDashBody.setProspectCount(totalProspectCount);
            crmDashBody.setCustomers(customerList);

            List<GraphBody> graphBodies = DsnUtils.getGraphBodies(
                    customerAmountGraph.entrySet()
                            .stream()
                            .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                            .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                    LinkedHashMap::new))
            );
            crmDashBody.setCustomerGraphBodies(graphBodies);

            return ResponseBody.with(crmDashBody, "Statistique CRM");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }

    }

}
