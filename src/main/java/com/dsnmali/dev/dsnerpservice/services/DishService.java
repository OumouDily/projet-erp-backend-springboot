package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.DishRepository;
import org.springframework.stereotype.Service;

@Service
public class DishService {
    private final DishRepository dishRepository;

    public DishService(DishRepository dishRepository) {
        this.dishRepository = dishRepository;
    }
}
