package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.SessionRepository;
import org.springframework.stereotype.Service;

@Service
public class SessionService {
    private final
    SessionRepository sessionRepository;

    public SessionService(SessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }
}
