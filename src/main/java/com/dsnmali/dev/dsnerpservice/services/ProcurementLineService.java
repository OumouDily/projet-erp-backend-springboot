package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.ProcurementLineRepository;
import org.springframework.stereotype.Service;

@Service
public class ProcurementLineService {

    final ProcurementLineRepository supplyLineRepository;

    public ProcurementLineService(ProcurementLineRepository supplyLineRepository) {
        this.supplyLineRepository = supplyLineRepository;
    }
}
