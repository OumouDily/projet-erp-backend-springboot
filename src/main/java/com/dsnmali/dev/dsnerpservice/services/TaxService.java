package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.Tax;
import com.dsnmali.dev.dsnerpservice.respositories.TaxRepository;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaxService {

    private final TaxRepository taxRepository;

    public TaxService(TaxRepository taxRepository) {
        this.taxRepository = taxRepository;
    }

    public ResponseBody getById(Long id) {
        try {
            return ResponseBody.with(taxRepository.findById(id), "Recuperer avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody save(Tax tax) {
        try {
            return ResponseBody.with(taxRepository.save(tax), "Ajoute avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody edit(Tax tax) {
        try {
            if (!taxRepository.findById(tax.getId()).isPresent()) {
                return ResponseBody.error("Cet Objet n'existe pas !");
            }
            return ResponseBody.with(taxRepository.save(tax), "Modifie avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody findAll() {
        try {
            List<Tax> taxes = taxRepository.findAll();
            return ResponseBody.with(taxes, taxes.size() + " taxes trouves");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue!");
        }
    }

}
