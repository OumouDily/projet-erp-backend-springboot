package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.Contact;
import com.dsnmali.dev.dsnerpservice.entities.Customer;
import com.dsnmali.dev.dsnerpservice.entities.CustomerContact;
import com.dsnmali.dev.dsnerpservice.respositories.ContactRepository;
import com.dsnmali.dev.dsnerpservice.respositories.CustomerContactRepository;
import com.dsnmali.dev.dsnerpservice.respositories.CustomerRepository;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ContactService {
    private final ContactRepository contactRepository;
    private final CustomerRepository customerRepository;
    private final CustomerContactRepository customerContactRepository;

    public ContactService(ContactRepository contactRepository,
                          CustomerRepository customerRepository, CustomerContactRepository customerContactRepository) {
        this.contactRepository = contactRepository;
        this.customerRepository = customerRepository;
        this.customerContactRepository = customerContactRepository;
    }

    public ResponseBody getById(Long id) {
        try {
            Optional<Contact> optionalContact = contactRepository.findById(id);
            if (!optionalContact.isPresent()) {
                return ResponseBody.error("Ce contact n'existe pas");
            }
            Contact contact = optionalContact.get();
            Set<Customer> customers = customerRepository.findAllByContactId(contact.getId());
            contact.setCustomers(customers);
            return ResponseBody.with(contact, "Contact recupere avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody save(Contact contact) {
        try {
            return ResponseBody.with(contactRepository.save(contact), "Contact ajoute avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody edit(Contact contact) {
        try {
            return ResponseBody.with(contactRepository.save(contact), "Contact modifie avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody delete(Long id) {
        try {
            Set<CustomerContact> customerContacts = customerContactRepository.findAllByContactId(id);
            customerContacts.forEach(customerContactRepository::delete);

            contactRepository.deleteById(id);

            return ResponseBody.success("Contact supprime avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody getAll(Long customerId) {
        try {
            if (customerId != null && customerId != 0) {
                Set<Contact> contacts = contactRepository.findAllByCustomerId(customerId);
                return ResponseBody.with(contacts, contacts.size() + " contact.s trouve.s");
            }
            List<Contact> contacts = contactRepository.findAll();
            return ResponseBody.with(contacts, contacts.size() + " contact.s trouves");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }
}
