package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.ProjectFolder;
import com.dsnmali.dev.dsnerpservice.respositories.ProjectFolderRepository;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import com.dsnmali.dev.dsnerpservice.utils.dashboard.ProjectFolderDashBody;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ProjectFolderService {

    private final ProjectFolderRepository projectFolderRepository;

    public ProjectFolderService(ProjectFolderRepository projectFolderRepository) {
        this.projectFolderRepository = projectFolderRepository;
    }


    public ResponseBody save(ProjectFolder folder) {
        try {
            projectFolderRepository.save(folder);
            return ResponseBody.with(folder, "Dossier projet ajoute avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }


    public ResponseBody edit(ProjectFolder folder) {
        try {
            if (projectFolderRepository.findById(folder.getId()).isPresent()) {
                projectFolderRepository.save(folder);
            }
            return ResponseBody.with(folder, "Dossier projet modifie avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }


    public ResponseBody list(SearchBody searchBody) {
        try {
            List<ProjectFolder> folders;
            if (searchBody.getCustomerId() != 0 && searchBody.getUserId() != 0) {
                folders = projectFolderRepository.findAllByCustomerIdAndAgentId(searchBody.getCustomerId(), searchBody.getUserId());
            } else if (searchBody.getCustomerId() != 0 && searchBody.getUserId() == 0) {
                folders = projectFolderRepository.findAllByCustomerId(searchBody.getCustomerId());
            } else if (searchBody.getCustomerId() == 0 && searchBody.getUserId() != 0) {
                folders = projectFolderRepository.findAllByAgentId(searchBody.getUserId());
            } else {
                folders = projectFolderRepository.findAll();
            }

            return ResponseBody.with(folders, folders.size() + "projects");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody getById(Long id) {
        try {
            Optional<ProjectFolder> optionalProjectFolder = projectFolderRepository.findById(id);
            if (!optionalProjectFolder.isPresent()) {
                return ResponseBody.error("Ce dossier projet n'existe pas");
            }

            return ResponseBody.with(projectFolderRepository.findById(id), "Recuperer avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody deleteById(Long id) {
        try {
            if (projectFolderRepository.findById(id).isPresent()) {
                projectFolderRepository.deleteById(id);
                return ResponseBody.success("Suppression avec succes");
            }
            return ResponseBody.error("Ce dossier projet n'existe pas");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue");
        }
    }

    public ResponseBody initProjectFolderDash(SearchBody searchBody) {

        try {

            int notStartedCount = 0;
            int inProgressCount = 0;
            int pendingCount = 0;
            int doneCount = 0;
            long sumNotStarted = 0;
            long sumInProgress = 0;
            long sumPending = 0;
            long sumDone = 0;


            List<ProjectFolder> projectFolderList;

            if (searchBody.getCreateById() != 0) {
                projectFolderList = projectFolderRepository.findAllByStateIsNotAndAgentId(Enumeration.PROJECT_FOLDER_STATE.CANCEL, searchBody.getCreateById());
            } else {
                projectFolderList = projectFolderRepository.findAll();
            }


            for (ProjectFolder pf : projectFolderList) {

                if (pf.getState() == Enumeration.PROJECT_FOLDER_STATE.TODO) {
                    notStartedCount++;
                    sumNotStarted += (pf.getTurnover() == null ? 0 : pf.getTurnover());
                }
                if (pf.getState() == Enumeration.PROJECT_FOLDER_STATE.IN_PROGRESS) {
                    inProgressCount++;
                    sumInProgress += (pf.getTurnover() == null ? 0 : pf.getTurnover());
                }
                if (pf.getState() == Enumeration.PROJECT_FOLDER_STATE.PENDING) {
                    pendingCount++;
                    sumPending += (pf.getTurnover() == null ? 0 : pf.getTurnover());
                }
                if (pf.getState() == Enumeration.PROJECT_FOLDER_STATE.DONE) {
                    doneCount++;
                    sumDone += (pf.getTurnover() == null ? 0 : pf.getTurnover());
                }

            }

            ProjectFolderDashBody projectFolderDashBody = new ProjectFolderDashBody();

            projectFolderDashBody.setNotStartedCount(notStartedCount);
            projectFolderDashBody.setInProgressCount(inProgressCount);
            projectFolderDashBody.setPendingCount(pendingCount);
            projectFolderDashBody.setDoneCount(doneCount);

            projectFolderDashBody.setSumNotStarted(sumNotStarted);
            projectFolderDashBody.setSumInProgress(sumInProgress);
            projectFolderDashBody.setSumPending(sumPending);
            projectFolderDashBody.setSumDone(sumDone);

            projectFolderDashBody.setProjectFolders(projectFolderList);


            return ResponseBody.with(projectFolderDashBody, "Statistique Projet");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }

    }

}
