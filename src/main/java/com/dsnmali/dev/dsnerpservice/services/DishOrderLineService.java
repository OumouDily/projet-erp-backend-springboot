package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.DishOrderLineRepository;
import org.springframework.stereotype.Service;

@Service
public class DishOrderLineService {

    final DishOrderLineRepository dishOrderLineRepository;

    public DishOrderLineService(DishOrderLineRepository dishOrderLineRepository) {
        this.dishOrderLineRepository = dishOrderLineRepository;
    }
}
