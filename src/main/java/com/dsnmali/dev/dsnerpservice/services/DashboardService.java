package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.*;
import com.dsnmali.dev.dsnerpservice.respositories.*;
import com.dsnmali.dev.dsnerpservice.utils.DsnUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import com.dsnmali.dev.dsnerpservice.utils.dashboard.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

@Service
public class DashboardService {

    private final InvoiceRepository invoiceRepository;
    private final InvoicePaymentRepository invoicePaymentRepository;
    private final CustomerRepository customerRepository;
    private final DevisRepository devisRepository;
    private final ProjectFolderRepository projectFolderRepository;
    private final UserRepository userRepository;

    public DashboardService(InvoiceRepository invoiceRepository,
                            InvoicePaymentRepository invoicePaymentRepository, ProjectFolderRepository projectFolderRepository,
                            DevisRepository devisRepository, CustomerRepository customerRepository,
                            UserRepository userRepository) {
        this.invoiceRepository = invoiceRepository;
        this.customerRepository = customerRepository;
        this.projectFolderRepository = projectFolderRepository;
        this.invoicePaymentRepository = invoicePaymentRepository;
        this.devisRepository = devisRepository;
        this.userRepository = userRepository;
    }

    public ResponseBody initCommercialDash(SearchBody searchBody) {
        try {

            Set<User> commercials = new HashSet<>();
            if (searchBody.getCreateById() != 0) {
                User commercial = userRepository.findUserById(searchBody.getCreateById());
                commercials.add(commercial);
            } else {
                commercials = getCommercials();
            }

            Set<CommercialDashItem> commercialDashItems = new LinkedHashSet<>();
            double achievedTurnover = 0;
            double pendingTurnover = 0;
            double objectiveTurnover = 0;
            double forecastTurnover = 0;

            for (User commercial : commercials) {

                InvoiceDashBody invoiceDashBody = getInvoiceDashBody(searchBody, commercial);
                CrmDashBody crmDashBody = getCrmDashBody(searchBody, commercial);
                ProjectFolderDashBody projectFolderDashBody = getProjectFolderDashBody(searchBody, commercial);

                CommercialDashItem commercialDashItem = new CommercialDashItem();
                commercialDashItem.setCommercial(commercial);
                commercialDashItem.setInvoiceDashBody(invoiceDashBody);
                commercialDashItem.setCrmDashBody(crmDashBody);
                commercialDashItem.setProjectFolderDashBody(projectFolderDashBody);

                commercialDashItem.setAchievedTurnover(invoiceDashBody.getAchievedTurnover());
                commercialDashItem.setPendingTurnover(invoiceDashBody.getTotalDevisOpen());
                commercialDashItem.setObjectiveTurnover((double) (commercial.getTurnoverTarget() == null ? 0 : commercial.getTurnoverTarget()));

                double sumForecastTurnover = projectFolderDashBody.getSumNotStarted() + projectFolderDashBody.getSumInProgress() + projectFolderDashBody.getSumPending();
                commercialDashItem.setForecastTurnover(sumForecastTurnover);

                achievedTurnover += commercialDashItem.getAchievedTurnover();
                pendingTurnover += commercialDashItem.getPendingTurnover();
                objectiveTurnover += commercialDashItem.getObjectiveTurnover();
                forecastTurnover += commercialDashItem.getForecastTurnover();

                commercialDashItems.add(commercialDashItem);
            }
            List<CommercialDashItem> commercialDashItemsSorted = new ArrayList<>(commercialDashItems);
            commercialDashItemsSorted.sort(Comparator.comparing(CommercialDashItem::getAchievedTurnover).reversed());


            CommercialDashBody commercialDashBody = new CommercialDashBody();
            commercialDashBody.setAchievedTurnover(achievedTurnover);
            commercialDashBody.setPendingTurnover(pendingTurnover);
            commercialDashBody.setObjectiveTurnover(objectiveTurnover);
            commercialDashBody.setForecastTurnover(forecastTurnover);
            commercialDashBody.setCommercialDashItems(new LinkedHashSet<>(commercialDashItemsSorted));

            return ResponseBody.with(commercialDashBody, "Statistique Commerciaux");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public Set<User> getCommercials() {

        Set<User> commercials = new HashSet<>();
        List<User> users = userRepository.findAll();
        users.stream().filter(User::isEnabled).forEach(user -> {
            user.getRoles().stream().filter(role -> role.getName().equalsIgnoreCase("COMMERCIAL")).filter(role -> !commercials.contains(user)).forEach(role -> commercials.add(user));
        });

        return commercials;
    }

    public InvoiceDashBody getInvoiceDashBody(SearchBody searchBody, User commercial) {

        InvoiceDashBody invoiceDashBody = new InvoiceDashBody();

        List<Invoice> invoices = new ArrayList<>();
        List<Devis> devisList = new ArrayList<>();

        if (StringUtils.isEmpty(searchBody.getYear()) && (searchBody.getStartDate() == null && searchBody.getEndDate() == null)) {
            invoices = invoiceRepository.findAllByStateIsNotAndAgentId(Enumeration.INVOICE_STATE.CANCEL, commercial.getId());
            devisList = devisRepository.findAllByStateIsNotAndAgentId(Enumeration.DEVIS_STATE.CANCEL, commercial.getId());

        } else if (!StringUtils.isEmpty(searchBody.getYear()) && (searchBody.getStartDate() == null && searchBody.getEndDate() == null)) {
            invoices = invoiceRepository.findAllByStateIsNotAndAgentIdAndYear(Enumeration.INVOICE_STATE.CANCEL, commercial.getId(),
                    DsnUtils.getYearDate(searchBody.getYear()));
            devisList = devisRepository.findAllByStateIsNotAndAgentIdAndYear(Enumeration.DEVIS_STATE.CANCEL, commercial.getId(),
                    DsnUtils.getYearDate(searchBody.getYear()));

        } else if (StringUtils.isEmpty(searchBody.getYear()) && (searchBody.getStartDate() != null && searchBody.getEndDate() != null)) {
            invoices = invoiceRepository.findAllByStateIsNotAndAgentIdAndDates(Enumeration.INVOICE_STATE.CANCEL, commercial.getId(),
                    searchBody.getStartDate(), searchBody.getEndDate());
            devisList = devisRepository.findAllByStateIsNotAndAgentIdAndDates(Enumeration.DEVIS_STATE.CANCEL, commercial.getId(),
                    searchBody.getStartDate(), searchBody.getEndDate());
        }

        // ================================== INVOICE STAT ======================================================== //
        // invoice count
        long sumAmountPaidCount = 0;
        long sumStayToPayCount = 0;
        long totalInvoiceCount = 0;

        double sumAmountPaid = 0;
        double sumStayToPay = 0;
        double totalInvoice = 0;
        double sumAchievedTurnover = 0;
        // double totalDiscount = 0;
        for (Invoice invoice : invoices) {
            if (invoice.isEnabled()) {

                if (invoice.getAmountPaid() != 0D) {
                    sumAmountPaidCount++;
                }
                if (invoice.getStayToPay() != 0D) {
                    sumStayToPayCount++;
                }
                totalInvoiceCount++;

                sumAmountPaid += invoice.getAmountPaid() == null ? 0 : invoice.getAmountPaid();
                sumStayToPay += invoice.getStayToPay() == null ? 0 : invoice.getStayToPay();
                totalInvoice += invoice.getTotal() == null ? 0 : invoice.getTotal();
                if (invoice.getDevis() != null && invoice.getDevis().getAgent() != null && invoice.getDevis().getAgent().equals(commercial)) {
                    sumAchievedTurnover += invoice.getTotal();
                }
                // totalDiscount += invoice.getDiscount() == null ? 0 : invoice.getDiscount();
            }
        }
        invoiceDashBody.setSumAmountPaidCount(sumAmountPaidCount);
        invoiceDashBody.setSumStayToPayCount(sumStayToPayCount);
        invoiceDashBody.setTotalInvoiceCount(totalInvoiceCount);
        invoiceDashBody.setSumAmountPaid(sumAmountPaid);
        invoiceDashBody.setSumStayToPay(sumStayToPay);
        invoiceDashBody.setTotalInvoice(totalInvoice);
        invoiceDashBody.setAchievedTurnover(sumAchievedTurnover);

        // ================================== DEVIS STAT ======================================================== //
        double totalDevisOpen = 0;
        long totalDevisOpenCount = 0;

        for (Devis devis : devisList) {
            if (devis.isEnabled()) {

                if (devis.getState().equals(Enumeration.DEVIS_STATE.OPEN)) {
                    totalDevisOpen += devis.getTotal() == null ? 0 : devis.getTotal();
                    totalDevisOpenCount++;
                }
            }
        }
        invoiceDashBody.setTotalDevisOpen(totalDevisOpen);
        invoiceDashBody.setTotalDevisOpenCount(totalDevisOpenCount);

        // TODO : INIT GRAPH INVOICE BODY
        // initInvoiceGraphBody(invoiceDashBody, searchBody);

        return invoiceDashBody;
    }

    public CrmDashBody getCrmDashBody(SearchBody searchBody, User commercial) {

        CrmDashBody crmDashBody = new CrmDashBody();

        // TODO : INIT CUSTOMER AMOUNT GRAPH
        // HashMap<String, Double> customerAmountGraph = new HashMap<>();

        int totalClientCount = 0;
        int totalProspectCount = 0;
        int newProspectCount = 0;
        int notContactedProspectCount = 0;
        Date startMonth = DsnUtils.subMonthToDate(new Date(), 1);

        List<Customer> customerList = customerRepository.getCustomers(commercial.getId());

        for (Customer c : customerList) {
            if (c.getType() == Enumeration.CUSTOMER_TYPE.PROSPECT) {
                if (c.getState() == Enumeration.CUSTOMER_STATE.NOT_CONTACTED) {
                    notContactedProspectCount++;
                }
                if (c.getCreateDate() != null && c.getCreateDate().after(startMonth)) {
                    newProspectCount++;
                }
                totalProspectCount++;
            } else if (c.getType() == Enumeration.CUSTOMER_TYPE.CLIENT) {
                totalClientCount++;
            }

            // Double sumAmount = invoiceRepository.sumTotalByCustomerIdAndStateNot(c.getId(), Enumeration.INVOICE_STATE.CANCEL);
            // customerAmountGraph.put(c.getName(), sumAmount == null ? 0 : sumAmount);
        }

        // PROSPECT CLIENT
        crmDashBody.setNewProspectCount(newProspectCount);
        crmDashBody.setNotContactedProspectCount(notContactedProspectCount);
        crmDashBody.setCustomerCount(totalClientCount);
        crmDashBody.setProspectCount(totalProspectCount);
        crmDashBody.setCustomers(customerList);

        /*List<GraphBody> graphBodies = DsnUtils.getGraphBodies(customerAmountGraph.entrySet()
                        .stream()
                        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                        .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new))
        );
        crmDashBody.setCustomerGraphBodies(graphBodies);*/

        return crmDashBody;
    }

    public ProjectFolderDashBody getProjectFolderDashBody(SearchBody searchBody, User commercial) {

        ProjectFolderDashBody projectFolderDashBody = new ProjectFolderDashBody();

        int notStartedCount = 0;
        int inProgressCount = 0;
        int pendingCount = 0;
        int doneCount = 0;
        long sumNotStarted = 0;
        long sumInProgress = 0;
        long sumPending = 0;
        long sumDone = 0;

        List<ProjectFolder> projectFolderList = new ArrayList<>();

        if (StringUtils.isEmpty(searchBody.getYear()) && (searchBody.getStartDate() == null && searchBody.getEndDate() == null)) {
            projectFolderList = projectFolderRepository.findAllByStateIsNotAndAgentId(Enumeration.PROJECT_FOLDER_STATE.CANCEL, commercial.getId());

        } else if (!StringUtils.isEmpty(searchBody.getYear()) && (searchBody.getStartDate() == null && searchBody.getEndDate() == null)) {
            projectFolderList = projectFolderRepository.findAllByStateIsNotAndAgentIdAndYear(Enumeration.PROJECT_FOLDER_STATE.CANCEL, commercial.getId(),
                    DsnUtils.getYearDate(searchBody.getYear()));

        } else if (StringUtils.isEmpty(searchBody.getYear()) && (searchBody.getStartDate() != null && searchBody.getEndDate() != null)) {
            projectFolderList = projectFolderRepository.findAllByStateIsNotAndAgentIdAndDates(Enumeration.PROJECT_FOLDER_STATE.CANCEL, commercial.getId(),
                    searchBody.getStartDate(), searchBody.getEndDate());
        }


        for (ProjectFolder pf : projectFolderList) {

            if (pf.getState() == Enumeration.PROJECT_FOLDER_STATE.TODO) {
                notStartedCount++;
                sumNotStarted += (pf.getTurnover() == null ? 0 : pf.getTurnover());
            }
            if (pf.getState() == Enumeration.PROJECT_FOLDER_STATE.IN_PROGRESS) {
                inProgressCount++;
                sumInProgress += (pf.getTurnover() == null ? 0 : pf.getTurnover());
            }
            if (pf.getState() == Enumeration.PROJECT_FOLDER_STATE.PENDING) {
                pendingCount++;
                sumPending += (pf.getTurnover() == null ? 0 : pf.getTurnover());
            }

            if (pf.getState() == Enumeration.PROJECT_FOLDER_STATE.DONE) {
                doneCount++;
                sumDone += (pf.getTurnover() == null ? 0 : pf.getTurnover());
            }

        }

        projectFolderDashBody.setNotStartedCount(notStartedCount);
        projectFolderDashBody.setInProgressCount(inProgressCount);
        projectFolderDashBody.setPendingCount(pendingCount);
        projectFolderDashBody.setDoneCount(doneCount);

        projectFolderDashBody.setSumNotStarted(sumNotStarted);
        projectFolderDashBody.setSumInProgress(sumInProgress);
        projectFolderDashBody.setSumPending(sumPending);
        projectFolderDashBody.setSumDone(sumDone);

        projectFolderDashBody.setProjectFolders(projectFolderList);


        return projectFolderDashBody;

    }

}
