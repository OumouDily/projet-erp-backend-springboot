package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.Project;
import com.dsnmali.dev.dsnerpservice.respositories.ProjectRepository;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {

    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public ResponseBody save(Project project) {
        try {
            projectRepository.save(project);
            return ResponseBody.with(project, "Project ajoute avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody findAllProjectByCustomerId(Long id) {
        try {
            return ResponseBody.with(projectRepository.findAllProjectByCustomerId(id), "Recuperer avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody findAllByInvoiceId(Long id) {
        try {
            return ResponseBody.with(projectRepository.findByInvoiceId(id), "Recuperer avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }


    public ResponseBody edit(Project project) {
        try {
            if (projectRepository.findById(project.getId()).isPresent()) {
                projectRepository.save(project);
            }
            return ResponseBody.with(project, "Project modifie avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }


    public ResponseBody getAll() {
        try {
            List<Project> projects = projectRepository.findAll();
            return ResponseBody.with(projects, projects.size() + "projects");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody remove(Long id) {
        try {
            if (projectRepository.findById(id).isPresent()) {
                projectRepository.deleteById(id);
            }
            return ResponseBody.success("Supprimer avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody getById(Long id) {
        try {
            return ResponseBody.with(projectRepository.findById(id), "Recuperer avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Erreur");
        }
    }

    public ResponseBody findAll() {
        try {
            List<Project> projects = projectRepository.findAll();
            return ResponseBody.with(projects, projects.size() + " projet.s trouve.s");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }


}
