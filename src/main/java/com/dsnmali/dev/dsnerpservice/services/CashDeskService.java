package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.CashDeskRepository;
import org.springframework.stereotype.Service;

@Service
public class CashDeskService {

    final CashDeskRepository cashDeskRepository;


    public CashDeskService(CashDeskRepository cashDeskRepository) {
        this.cashDeskRepository = cashDeskRepository;
    }
}
