package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.components.ParameterComponent;
import com.dsnmali.dev.dsnerpservice.entities.Devis;
import com.dsnmali.dev.dsnerpservice.entities.Invoice;
import com.dsnmali.dev.dsnerpservice.entities.InvoiceLine;
import com.dsnmali.dev.dsnerpservice.entities.Project;
import com.dsnmali.dev.dsnerpservice.respositories.DevisRepository;
import com.dsnmali.dev.dsnerpservice.respositories.InvoicePaymentRepository;
import com.dsnmali.dev.dsnerpservice.respositories.InvoiceRepository;
import com.dsnmali.dev.dsnerpservice.respositories.ProjectRepository;
import com.dsnmali.dev.dsnerpservice.utils.DsnUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import com.dsnmali.dev.dsnerpservice.utils.dashboard.GraphBody;
import com.dsnmali.dev.dsnerpservice.utils.dashboard.InvoiceDashBody;
import com.dsnmali.dev.dsnerpservice.wrapper.InvoiceSaveEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.partitioningBy;

@Service
public class InvoiceService {

    final InvoiceRepository invoiceRepository;
    final InvoiceLineService invoiceLineService;
    final InvoicePaymentRepository invoicePaymentRepository;
    final DevisService devisService;
    private final DevisRepository devisRepository;
    private final ParameterComponent parameterComponent;
    private final ProjectRepository projectRepository;

    public InvoiceService(InvoiceRepository invoiceRepository,
                          InvoiceLineService invoiceLineService,
                          DevisService devisService,
                          InvoicePaymentRepository invoicePaymentRepository,
                          ProjectRepository projectRepository,
                          DevisRepository devisRepository,
                          ParameterComponent parameterComponent) {
        this.invoiceRepository = invoiceRepository;
        this.invoiceLineService = invoiceLineService;
        this.devisService = devisService;
        this.parameterComponent = parameterComponent;
        this.projectRepository = projectRepository;
        this.invoicePaymentRepository = invoicePaymentRepository;
        this.devisRepository = devisRepository;
    }

    public ResponseBody findAllInvoiceNotPaid() {
        try {
            return ResponseBody.with(invoiceRepository.findAllInvoiceByPaid(false), "Liste de facture non payee !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody findAll() {
        try {
            return ResponseBody.with(invoiceRepository.findAllInvoiceByEnabled(true), "Invoice recuperer avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public Page<Invoice> getAll(Pageable pageable) {
        return invoiceRepository.findAllInvoiceByEnabled(true, pageable);
    }

    public Page<Invoice> list(SearchBody searchBody) {
        Pageable pageable = PageRequest.of(searchBody.getPage().orElse(0), 30,
                Sort.Direction.ASC, StringUtils.isEmpty(searchBody.getSortBy()) ? "id" : searchBody.getSortBy().toString());

        if (searchBody.getCustomerId() != 0 && searchBody.getUserId() != 0) {
            if (searchBody.getInvoiceState() != null) {
                return invoiceRepository.findAllInvoiceByEnabledAndCustomerIdAndAgentIdAndState(true, pageable, searchBody.getCustomerId(), searchBody.getUserId(),
                        searchBody.getInvoiceState());

            } else {
                return invoiceRepository.findAllInvoiceByEnabledAndCustomerIdAndAgentId(true, pageable, searchBody.getCustomerId(),
                        searchBody.getUserId());
            }
        } else if (searchBody.getCustomerId() != 0 && searchBody.getUserId() == 0) {
            if (searchBody.getInvoiceState() != null) {
                return invoiceRepository.findAllInvoiceByEnabledAndCustomerIdAndState(true, pageable, searchBody.getCustomerId(), searchBody.getInvoiceState());
            } else {
                return invoiceRepository.findAllInvoiceByEnabledAndCustomerId(true, pageable, searchBody.getCustomerId());
            }
        } else if (searchBody.getCustomerId() == 0 && searchBody.getUserId() != 0) {
            if (searchBody.getInvoiceState() != null) {
                return invoiceRepository.findAllInvoiceByEnabledAndAgentIdAndState(true, pageable, searchBody.getUserId(), searchBody.getInvoiceState());
            } else {
                return invoiceRepository.findAllInvoiceByEnabledAndAgentId(true, pageable, searchBody.getUserId());
            }
        } else {
            if (searchBody.getInvoiceState() != null) {
                return invoiceRepository.findAllInvoiceByEnabledAndState(true, pageable, searchBody.getInvoiceState());
            } else {
                return invoiceRepository.findAllInvoiceByEnabled(true, pageable);
            }
        }

    }

    public ResponseBody getById(Long id) {
        try {
            Invoice invoice = invoiceRepository.findInvoiceById(id);
            if (invoice == null) {
                return ResponseBody.error("Cette facture n'existe pas");
            }
            String amountInLetter = DsnUtils.convertNumberToWord(invoice.getTotal()) + " (" + DsnUtils.formatNumber(invoice.getTotal()).replaceAll("\\s", ".") + ")".toLowerCase();
            invoice.setAmountInLetter(amountInLetter);
            return ResponseBody.with(invoice, "Invoice recuperer avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody createInvoice(InvoiceSaveEntity invoiceSaveEntity) {
        try {
            if (isEmpty(invoiceSaveEntity.getInvoiceLines()) || invoiceRepository.existsByNumber(invoiceSaveEntity.getInvoice().getNumber())) {
                return ResponseBody.error("Liste vide oubien ce numero existe deja !");
            }

            boolean hasValideInvoice = invoiceRepository.existsByDevisIdAndEnabled(invoiceSaveEntity.getInvoice().getDevis().getId(), true);
            if (hasValideInvoice) {
                return ResponseBody.error("Ce devis est deja facture");
            }

            invoiceSaveEntity.getInvoice().setMethodOfPayment(Enumeration.METHOD_OF_PAYMENT.CHECK);
            Invoice invoice = invoiceSaveEntity.getInvoice();

            // set invoice number
            invoice.setNumber(parameterComponent.generateInvoiceNumber(Enumeration.INVOICE_TYPE.FACT));

            // set invoice  stat to pay
            invoice.setStayToPay(invoice.getTotal());

            // invoice
            Invoice newInvoice = invoiceRepository.saveAndFlush(invoice);

            // update parameter.lastInvoiceNumber
            parameterComponent.updateInvoiceNumber(Enumeration.INVOICE_TYPE.FACT);

            // invoice lines
            invoiceSaveEntity.getInvoiceLines()
                    .stream()
                    .peek(line -> setterInvoice(line, newInvoice))
                    .forEach(this::saveLine);

            Devis devis = devisRepository.findDevisById(newInvoice.getDevis().getId());
            if (devis != null) {
                devis.setState(Enumeration.DEVIS_STATE.INVOICED);
                devis.setInvoiceGenerated(true);
                devis.setAmountInvoiced(newInvoice.getTotal());
                devisRepository.save(devis);
            }

            return ResponseBody.with(newInvoice, "Ajout avec succes !");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody updateInvoice(InvoiceSaveEntity invoiceSaveEntity) {
        try {

            if (isEmpty(invoiceSaveEntity.getInvoiceLines())) {
                return ResponseBody.error("Liste vide oubien");
            }

            Invoice invoice = invoiceSaveEntity.getInvoice();

            // amount
            assignAmount(invoice, invoiceSaveEntity.getInvoiceLines());

            // set invoice stay to pay
            invoice.setStayToPay(invoice.getTotal());

            // invoice
            Invoice updatedInvoice = invoiceRepository.saveAndFlush(invoice);

            List<InvoiceLine> invoiceLineList = this.invoiceLineService.findListByInvoice(updatedInvoice.getId());

            List<Long> invoiceLinesExisting = invoiceSaveEntity.getInvoiceLines()
                    .stream()
                    .filter(invoiceLine -> invoiceLine.getId() != null)
                    .map(InvoiceLine::getId)
                    .collect(Collectors.toList());

            this.invoiceLineService.deleteAllInvoiceLine(
                    Optional.ofNullable(
                            invoiceLineList
                                    .stream()
                                    .collect(partitioningBy(p -> invoiceLinesExisting.contains(p.getId())))
                                    .get(false)
                    ).orElse(new ArrayList<>())
            );

            // invoice lines
            invoiceSaveEntity.getInvoiceLines()
                    .stream()
                    .peek(line -> setterInvoice(line, updatedInvoice))
                    .forEach(this::saveLine);

            // update amount invoiced on devis
            if (updatedInvoice.getDevis() != null) {
                updatedInvoice.getDevis().setState(Enumeration.DEVIS_STATE.INVOICED);
                updatedInvoice.getDevis().setInvoiceGenerated(true);
                updatedInvoice.getDevis().setAmountInvoiced(updatedInvoice.getTotal());
                devisRepository.save(updatedInvoice.getDevis());
            }

            return ResponseBody.with(updatedInvoice, "Modifie avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody validateAnInvoice(Invoice invoice) {
        // System.out.println("Validate an Invoice : " + invoice.toString());
        try {

            invoice.setEditable(false);
            invoice.setState(Enumeration.INVOICE_STATE.VALIDATE);
            Invoice validatedInvoice = invoiceRepository.save(invoice);

            // update amount invoiced on devis
            if (validatedInvoice.getDevis() != null) {
                validatedInvoice.getDevis().setAmountInvoiced(validatedInvoice.getTotal());
                devisRepository.save(validatedInvoice.getDevis());
            }

            // project
            if (!projectRepository.existsByInvoiceId(invoice.getId())) {
                Project project = new Project();
                project.setTitle(invoice.getTitle());
                project.setInvoice(invoice);
                project.setAmount(invoice.getTotal());
                project.setStartDate(new Date());
                projectRepository.save(project);
            }

            return ResponseBody.with(validatedInvoice, "Facture validee avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    //Modification of the invoice after payment
    public ResponseBody editInvoice(Invoice invoice) {
        try {
            Invoice updatedInvoice = invoiceRepository.findInvoiceById(invoice.getId());
            if (updatedInvoice == null) {
                return ResponseBody.error("Cette facture n'existe pas");
            }

            updatedInvoice = invoiceRepository.save(invoice);

            // update amount invoiced on devis
            if (updatedInvoice.getDevis() != null) {
                updatedInvoice.getDevis().setAmountInvoiced(updatedInvoice.getTotal());
                devisRepository.save(updatedInvoice.getDevis());
            }

            return ResponseBody.with(updatedInvoice, "Facture mise à jour avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody cancelInvoice(Invoice invoice) {
        try {
            if (invoiceRepository.findInvoiceById(invoice.getId()) == null) {
                return ResponseBody.error("Cette facture n'existe pas");
            }
            invoice.setState(Enumeration.INVOICE_STATE.CANCEL);
            invoice.setEditable(false);

            invoice = invoiceRepository.save(invoice);

            // make devis duplicable
            Devis devis = devisRepository.findDevisById(invoice.getDevis().getId());
            if (devis != null) {
                devis.setCanDuplicate(true);
                devisRepository.save(devis);
            }

            return ResponseBody.with(invoice, "Facture annulee avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody deleteInvoice(Long id) {
        try {
            Invoice invoice = invoiceRepository.findInvoiceById(id);
            if (invoice == null) {
                return ResponseBody.error("Cette facture n'existe pas");
            }
            invoice.setEnabled(false);
            invoiceRepository.save(invoice);

            if (invoice.getDevis() != null) {
                invoice.getDevis().setState(Enumeration.DEVIS_STATE.OPEN);
                invoice.getDevis().setCanDuplicate(false);
                invoice.getDevis().setInvoiceGenerated(false);
                devisRepository.save(invoice.getDevis());
            }

            return ResponseBody.success("Facture supprimee avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    private void assignAmount(Invoice invoice, List<InvoiceLine> invoiceLines) {
        invoice.setSubTotal(
                Optional.of(invoiceLines
                        .stream()
                        .map(lineFactor -> lineFactor.getAmount().toString())
                        .mapToDouble(Double::parseDouble)
                        .sum())
                        .orElse(0.0)
        );
    }

    public ResponseBody getInvoicesByPaid(boolean paid) {
        try {
            return ResponseBody.with(invoiceRepository.findAllByPaidAndEditable(paid, paid), "Facture non payee !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    private void saveLine(InvoiceLine invoiceLine) {
        this.invoiceLineService.create(invoiceLine);
    }

    private void setterInvoice(InvoiceLine invoiceLine, Invoice invoice) {
        invoiceLine.setInvoice(invoice);
    }

    private boolean isEmpty(Collection<InvoiceLine> collections) {
        return collections == null || collections.isEmpty();
    }

    public ResponseBody initInvoiceDash(SearchBody searchBody) {
        try {

            InvoiceDashBody invoiceDashBody = new InvoiceDashBody();

            // retreive uncanceled invoice
            List<Invoice> invoices;
            // retreive uncanceled devis
            List<Devis> devisList;
            if (searchBody.getCreateById() != 0) {
                invoices = invoiceRepository.findAllByStateIsNotAndAgentId(Enumeration.INVOICE_STATE.CANCEL, searchBody.getCreateById());
                devisList = devisRepository.findAllByStateIsNotAndAgentId(Enumeration.DEVIS_STATE.CANCEL, searchBody.getCreateById());
            } else {
                invoices = invoiceRepository.findAllByStateIsNot(Enumeration.INVOICE_STATE.CANCEL);
                devisList = devisRepository.findAllByStateIsNot(Enumeration.DEVIS_STATE.CANCEL);
            }

            // ================================== INVOICE STAT ======================================================== //
            // invoice count
            long sumAmountPaidCount = 0;
            long sumStayToPayCount = 0;
            long totalInvoiceCount = 0;

            double sumAmountPaid = 0;
            double sumStayToPay = 0;
            double totalInvoice = 0;
            // double totalDiscount = 0;
            for (Invoice invoice : invoices) {
                if (invoice.isEnabled()) {

                    if (invoice.getAmountPaid() != 0D) {
                        sumAmountPaidCount++;
                    }
                    if (invoice.getStayToPay() != 0D) {
                        sumStayToPayCount++;
                    }
                    totalInvoiceCount++;

                    sumAmountPaid += invoice.getAmountPaid() == null ? 0 : invoice.getAmountPaid();
                    sumStayToPay += invoice.getStayToPay() == null ? 0 : invoice.getStayToPay();
                    totalInvoice += invoice.getTotal() == null ? 0 : invoice.getTotal();
                    // totalDiscount += invoice.getDiscount() == null ? 0 : invoice.getDiscount();
                }
            }
            invoiceDashBody.setSumAmountPaidCount(sumAmountPaidCount);
            invoiceDashBody.setSumStayToPayCount(sumStayToPayCount);
            invoiceDashBody.setTotalInvoiceCount(totalInvoiceCount);
            invoiceDashBody.setSumAmountPaid(sumAmountPaid);
            invoiceDashBody.setSumStayToPay(sumStayToPay);
            invoiceDashBody.setTotalInvoice(totalInvoice);

            // ================================== DEVIS STAT ======================================================== //
            double totalDevisOpen = 0;
            long totalDevisOpenCount = 0;

            for (Devis devis : devisList) {
                if (devis.isEnabled()) {

                    if (devis.getState().equals(Enumeration.DEVIS_STATE.OPEN)) {
                        totalDevisOpen += devis.getTotal() == null ? 0 : devis.getTotal();
                        totalDevisOpenCount++;
                    }
                }
            }
            invoiceDashBody.setTotalDevisOpen(totalDevisOpen);
            invoiceDashBody.setTotalDevisOpenCount(totalDevisOpenCount);

            // graph body
            initInvoiceGraphBody(invoiceDashBody, searchBody);

            return ResponseBody.with(invoiceDashBody, "Statistique Facturation");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    private void initInvoiceGraphBody(InvoiceDashBody invoiceDashBody, SearchBody searchBody) {
        try {
            List<GraphBody> graphBodies = new ArrayList<>();

            List<Date> last12months = DsnUtils.getLast12Months();
            for (Date date : last12months) {
                String month = DsnUtils.toString(date, "MMMM-yyyy", Locale.FRENCH);
                Double sumTotal;
                if (searchBody.getCreateById() != 0) {
                    sumTotal = invoiceRepository.sumTotalByInvoiceDateAndStateNotAndAgentId(date, Enumeration.INVOICE_STATE.CANCEL,
                            searchBody.getCreateById());
                } else {
                    sumTotal = invoiceRepository.sumTotalByInvoiceDateAndStateNot(date, Enumeration.INVOICE_STATE.CANCEL);
                }
                graphBodies.add(new GraphBody(month.toUpperCase(), sumTotal == null ? 0 : sumTotal));
            }

            invoiceDashBody.setInvoiceGraphBodies(graphBodies);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
