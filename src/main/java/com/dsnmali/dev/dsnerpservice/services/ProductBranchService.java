package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.ProductBranchRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductBranchService {
    private final
    ProductBranchRepository productBranchRepository;

    public ProductBranchService(ProductBranchRepository productBranchRepository) {
        this.productBranchRepository = productBranchRepository;
    }
}
