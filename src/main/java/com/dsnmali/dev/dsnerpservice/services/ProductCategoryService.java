package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.Product;
import com.dsnmali.dev.dsnerpservice.entities.ProductCategory;
import com.dsnmali.dev.dsnerpservice.respositories.ProductCategoryRepository;
import com.dsnmali.dev.dsnerpservice.respositories.ProductRepository;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ProductCategoryService {

    final ProductCategoryRepository productCategoryRepository;
    final ProductRepository productRepository;

    public ProductCategoryService(ProductCategoryRepository productCategoryRepository,
                                  ProductRepository productRepository) {
        this.productCategoryRepository = productCategoryRepository;
        this.productRepository = productRepository;
    }

    public ResponseBody getById(Long id) {
        try {
            Optional<ProductCategory> optionalProductCategory = productCategoryRepository.findById(id);
            if (!optionalProductCategory.isPresent()) {
                return ResponseBody.error("Cette categorie n'existe pas");
            }
            ProductCategory productCategory = optionalProductCategory.get();
            Set<Product> products = productRepository.findAllByProductCategoryId(productCategory.getId());
            productCategory.setProducts(products);
            return ResponseBody.with(productCategory, "Categorie recupere avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody save(ProductCategory productCategory) {
        try {
            return ResponseBody.with(productCategoryRepository.save(productCategory), "Categorie ajoute avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody edit(ProductCategory productCategory) {
        try {
            return ResponseBody.with(productCategoryRepository.save(productCategory), "Categorie modifie avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody delete(Long id) {
        try {
            Set<Product> products = productRepository.findAllByProductCategoryId(id);
            products.forEach(productRepository::delete);

            productCategoryRepository.deleteById(id);

            return ResponseBody.success("Categorie supprime avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody getAll() {
        try {
            List<ProductCategory> productCategories = productCategoryRepository.findAll();
            return ResponseBody.with(productCategories, productCategories.size() + " categorie.s trouve.s");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }
}
