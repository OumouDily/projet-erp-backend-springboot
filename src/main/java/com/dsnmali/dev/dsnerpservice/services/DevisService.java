package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.components.ParameterComponent;
import com.dsnmali.dev.dsnerpservice.entities.Devis;
import com.dsnmali.dev.dsnerpservice.entities.DevisLine;
import com.dsnmali.dev.dsnerpservice.entities.DevisLog;
import com.dsnmali.dev.dsnerpservice.respositories.DevisLogRepository;
import com.dsnmali.dev.dsnerpservice.respositories.DevisRepository;
import com.dsnmali.dev.dsnerpservice.utils.DsnUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import com.dsnmali.dev.dsnerpservice.wrapper.DevisSaveEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.partitioningBy;

@Service
public class DevisService {

    private final DevisRepository devisRepository;
    private final DevisLogRepository devisLogRepository;
    private final ParameterComponent parameterComponent;
    private final DevisLineService devisLineService;

    public DevisService(DevisRepository devisRepository, DevisLineService devisLineService,
                        DevisLogRepository devisLogRepository, ParameterComponent parameterComponent) {
        this.devisRepository = devisRepository;
        this.devisLineService = devisLineService;
        this.devisLogRepository = devisLogRepository;
        this.parameterComponent = parameterComponent;
    }

    public ResponseBody findAll() {
        try {
            return ResponseBody.with(devisRepository.findAll(), "Devis recuperer avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody list(SearchBody searchBody) {
        try {
            List<Devis> devis;
            if (searchBody.getCustomerId() != 0 && searchBody.getUserId() != 0) {
                if (searchBody.getDevisState() != null) {
                    devis = devisRepository.findAllByCustomerIdAndAgentIdAndState(searchBody.getCustomerId(), searchBody.getUserId(),
                            searchBody.getDevisState());

                } else {
                    devis = devisRepository.findAllByCustomerIdAndAgentId(searchBody.getCustomerId(), searchBody.getUserId());
                }
            } else if (searchBody.getCustomerId() != 0 && searchBody.getUserId() == 0) {
                if (searchBody.getDevisState() != null) {
                    devis = devisRepository.findAllByCustomerIdAndState(searchBody.getCustomerId(), searchBody.getDevisState());
                } else {
                    devis = devisRepository.findAllByCustomerId(searchBody.getCustomerId());
                }
            } else if (searchBody.getCustomerId() == 0 && searchBody.getUserId() != 0) {
                if (searchBody.getDevisState() != null) {
                    devis = devisRepository.findAllByAgentIdAndState(searchBody.getUserId(), searchBody.getDevisState());
                } else {
                    devis = devisRepository.findAllByAgentId(searchBody.getUserId());
                }
            } else {
                if (searchBody.getDevisState() != null) {
                    devis = devisRepository.findAllByState(searchBody.getDevisState());
                } else {
                    devis = devisRepository.findAll();
                }
            }

            return ResponseBody.with(devis, devis.size() + "devis");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody getById(Long id) {
        try {
            Devis devis = devisRepository.findDevisById(id);
            if (devis == null) {
                return ResponseBody.error("Ce devis n'existe pas");
            }
            String amountInLetter = DsnUtils.convertNumberToWord(devis.getTotal()) + " (" + new String(DsnUtils.formatNumber(devis.getTotal())).replaceAll("\\s", ".") + ")".toLowerCase();
            devis.setAmountInLetter(amountInLetter);
            return ResponseBody.with(devis, "Devis recuperer avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody createDevis(DevisSaveEntity devisSaveEntity) {
        try {
            // System.out.println("DevisSaveEntity : " + devisSaveEntity.toString());
            if (isEmpty(devisSaveEntity.getDevisLines()) || devisRepository.existsByNumber(devisSaveEntity.getDevis().getNumber())) {
                return ResponseBody.error("Liste vide oubien ce numero existe deja !");
            }

            //SUBTOTAL
            assignSubTotal(devisSaveEntity.getDevis(), devisSaveEntity.getDevisLines());

            // set devis number
            devisSaveEntity.getDevis().setNumber(parameterComponent.generateInvoiceNumber(Enumeration.INVOICE_TYPE.DEVIS));

            //DEVIS
            Devis devis = devisRepository.saveAndFlush(devisSaveEntity.getDevis());

            // update parameter.lastDevisNumber
            parameterComponent.updateInvoiceNumber(Enumeration.INVOICE_TYPE.DEVIS);

            // DEVISLINES
            devisSaveEntity.getDevisLines()
                    .stream()
                    .peek(devisLine -> setterDevis(devisLine, devisSaveEntity.getDevis()))
                    .forEach(this::saveDevisLine);
            return ResponseBody.with(devis, "Devis Ajoute avec succes !");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody updateDevis(DevisSaveEntity devisSaveEntity) {
        try {
            System.out.println("DevisSaveEntity : " + devisSaveEntity.toString());
            if (isEmpty(devisSaveEntity.getDevisLines())) {
                return ResponseBody.error("Liste vide oubien!");
            }

            //SUBTOTAL
            assignSubTotal(devisSaveEntity.getDevis(), devisSaveEntity.getDevisLines());

            //DEVIS
            devisSaveEntity.getDevis().setUpdateDate(new Date());
            Devis devis = devisRepository.saveAndFlush(devisSaveEntity.getDevis());

            DevisLog devisLog = new DevisLog("DEVIS-" + devis.getNumber(), new Date(), devis.getComment());

            //SAVE QUOTE
            devisLogRepository.save(devisLog);

            List<DevisLine> devisLineList = this.devisLineService.findListByDevis(devis.getId());

            List<Long> devisLinesExisting = devisSaveEntity.getDevisLines()
                    .stream()
                    .filter(devisLine -> devisLine.getId() != null)
                    .map(DevisLine::getId)
                    .collect(Collectors.toList());

            this.devisLineService.deleteAllDevisLine(
                    Optional.ofNullable(
                            devisLineList
                                    .stream()
                                    .collect(partitioningBy(p -> devisLinesExisting.contains(p.getId())))
                                    .get(false)
                    ).orElse(new ArrayList<>())
            );

            //DEVISLINES
            devisSaveEntity.getDevisLines()
                    .stream()
                    .peek(devisLine -> setterDevis(devisLine, devisSaveEntity.getDevis()))
                    .forEach(this::saveDevisLine);

            return ResponseBody.with(devis, "Modifie avec succes!");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }

    }

    public ResponseBody duplicate(DevisSaveEntity devisSaveEntity) {
        try {
            final Devis devis = devisRepository.findDevisById(devisSaveEntity.getDevis().getId());
            if (devis == null) {
                return ResponseBody.error("Ce devis n'existe pas");
            }
            devis.setCanDuplicate(false);
            devis.setState(Enumeration.DEVIS_STATE.DUPLICATE);
            devisRepository.save(devis);

            // prepare new devis
            final Devis newDevis = devisSaveEntity.getDevis();
            newDevis.setId(null);
            newDevis.setCanDuplicate(false);
            newDevis.setInvoiceGenerated(false);
            newDevis.setState(Enumeration.DEVIS_STATE.OPEN);
            newDevis.setAmountInvoiced(null);
            // set devis number
            newDevis.setNumber(parameterComponent.generateInvoiceNumber(Enumeration.INVOICE_TYPE.DEVIS));
            // SUBTOTAL
            assignSubTotal(newDevis, devisSaveEntity.getDevisLines());

            devisRepository.saveAndFlush(newDevis);

            // update last devis number in parameter
            parameterComponent.updateInvoiceNumber(Enumeration.INVOICE_TYPE.DEVIS);

            // save lines devis
            devisSaveEntity.getDevisLines()
                    .stream()
                    .peek(devisLine -> setterDevisForUpdate(devisLine, newDevis))
                    .forEach(this::saveDevisLine);

            return ResponseBody.with(newDevis, "Devis duplique avec succes !");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody cancelDevis(Devis devis) {
        try {
            if (devisRepository.findDevisById(devis.getId()) == null) {
                return ResponseBody.error("Ce devis n'existe pas");
            }
            devis.setState(Enumeration.DEVIS_STATE.CANCEL);
            devisRepository.save(devis);

            return ResponseBody.with(devis, "Devis annule avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody deleteDevis(Long id) {
        try {
            Devis devis = devisRepository.findDevisById(id);
            if (devis == null) {
                return ResponseBody.error("Ce devis n'existe pas");
            }
            devis.setEnabled(false);
            devisRepository.save(devis);

            return ResponseBody.with(devis, "Facture supprimee avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody getDevisByGenerated(boolean generated) {
        try {
            return ResponseBody.with(devisRepository.findAllByInvoiceGenerated(generated), "Devis recuperer avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    private void assignSubTotal(Devis devis, List<DevisLine> devisLines) {
        devis.setSubTotal(
                Optional.of(devisLines
                        .stream()
                        .map(devisLine -> devisLine.getAmount().toString())
                        .mapToDouble(Double::parseDouble)
                        .sum())
                        .orElse(0.0)
        );
    }

    private void saveDevisLine(DevisLine devisLine) {
        this.devisLineService.create(devisLine);
    }

    private void setterDevis(DevisLine devisLine, Devis devis) {
        devisLine.setDevis(devis);
    }

    private void setterDevisForUpdate(DevisLine devisLine, Devis devis) {
        devisLine.setId(null);
        devisLine.setDevis(devis);
    }

    private boolean isEmpty(Collection<DevisLine> collections) {
        return collections == null || collections.isEmpty();
    }


}
