package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.BranchActivity;
import com.dsnmali.dev.dsnerpservice.respositories.BranchActivityRepository;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BranchActivityService {
    private final BranchActivityRepository branchActivityRepository;

    public BranchActivityService(BranchActivityRepository branchActivityRepository) {
        this.branchActivityRepository = branchActivityRepository;
    }

    public ResponseBody getById(Long id) {
        try {
            return ResponseBody.with(branchActivityRepository.findById(id), "Recuperer avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }

    public ResponseBody save(BranchActivity branchActivity) {
        try {
            return ResponseBody.with(branchActivityRepository.save(branchActivity), "Ajoute avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody edit(BranchActivity branchActivity) {
        try {
            if (!branchActivityRepository.findById(branchActivity.getId()).isPresent()) {
                System.out.println("Service Id : " + branchActivity.getId());
                return ResponseBody.error("Cet Objet n'existe pas !");
            }
            return ResponseBody.with(branchActivityRepository.save(branchActivity), "Modifie avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody listOfSectorOfActivities() {
        try {
            List<BranchActivity> branchActivities = branchActivityRepository.findAll();
            return ResponseBody.with(branchActivities, branchActivities.size() + " Secteur d'activite trouves");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }


    public ResponseBody getSectorObjFromName(String name) {
        try {
            return ResponseBody.with(branchActivityRepository.findByName(name), "Recuperer avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Ce nom existe deja!");
        }
    }
}
