package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.FileDocRepository;
import org.springframework.stereotype.Service;

@Service
public class FileDocService {

    final FileDocRepository fileDocRepository;

    public FileDocService(FileDocRepository fileDocRepository) {
        this.fileDocRepository = fileDocRepository;
    }
}
