package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.entities.InvoiceLine;
import com.dsnmali.dev.dsnerpservice.respositories.InvoiceLineRepository;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceLineService {

    final InvoiceLineRepository invoiceLineRepository;


    public InvoiceLineService(InvoiceLineRepository invoiceLineRepository) {
        this.invoiceLineRepository = invoiceLineRepository;
    }

    public ResponseBody create(InvoiceLine invoiceLine) {
        // System.out.println("Ligne invoice : " + invoiceLine.toString());
        try {
            return ResponseBody.with(invoiceLineRepository.save(invoiceLine), "Ajoute avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public List<InvoiceLine> findListByInvoice(Long id) {
        return invoiceLineRepository.findByInvoice(id);
    }

    public ResponseBody findInvoiceLine(Long id) {
        try {
            return ResponseBody.with(invoiceLineRepository.findInvoiceLine(id), "Recuperer avec succes!");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public void deleteAllInvoiceLine(List<InvoiceLine> invoiceLines) {
        invoiceLineRepository.deleteAll(invoiceLines);
    }
}
