package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.DevisLogRepository;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

@Service
public class DevisLogService {
    private final DevisLogRepository devisLogRepository;

    public DevisLogService(DevisLogRepository devisLogRepository) {
        this.devisLogRepository = devisLogRepository;
    }

    public ResponseBody
    findAllQuote() {
        try {
            return ResponseBody.with(devisLogRepository.findAll(), "Recuperer avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }
}
