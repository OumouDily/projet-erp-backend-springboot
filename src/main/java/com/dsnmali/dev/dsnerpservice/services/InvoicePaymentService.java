package com.dsnmali.dev.dsnerpservice.services;


import com.dsnmali.dev.dsnerpservice.entities.Invoice;
import com.dsnmali.dev.dsnerpservice.entities.InvoicePayment;
import com.dsnmali.dev.dsnerpservice.respositories.InvoicePaymentRepository;
import com.dsnmali.dev.dsnerpservice.respositories.InvoiceRepository;
import com.dsnmali.dev.dsnerpservice.utils.DsnUtils;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class InvoicePaymentService {

    final InvoicePaymentRepository invoicePaymentRepository;
    final InvoiceService invoiceService;
    final InvoiceRepository invoiceRepository;

    String MESSAGE_OK;
    String MESSAGE_CANCEL;

    public InvoicePaymentService(InvoicePaymentRepository invoicePaymentRepository,
                                 InvoiceService invoiceService,
                                 InvoiceRepository invoiceRepository) {
        this.invoicePaymentRepository = invoicePaymentRepository;
        this.invoiceService = invoiceService;
        this.invoiceRepository = invoiceRepository;
    }

    public ResponseBody validate(InvoicePayment invoicePayment) {
        try {

            Invoice invoice = invoiceRepository.findInvoiceById(invoicePayment.getInvoice().getId());
            if (invoice == null) {
                return ResponseBody.error("Cette facture n'existe pas");
            }

            List<InvoicePayment> invoicePayments = invoicePaymentRepository.findAllByIdLessThanAndInvoiceIdAndCanceledAndStateEquals(
                    invoicePayment.getId(), invoice.getId(), false, Enumeration.PAYMENT_STATE.PENDING);
            if (invoicePayments != null && !invoicePayments.isEmpty()) {
                return ResponseBody.error("Impossible de valider ce paiement\nVous devez d'abord valider les enregistrements plus anciens");
            }

            InvoicePayment oldPayment = invoicePaymentRepository.findInvoicePaymentById(invoicePayment.getId());
            if (oldPayment == null) {
                return ResponseBody.error("Ce paiement n'existe pas");
            }
            oldPayment.setReference(invoicePayment.getReference());
            oldPayment.setPaymentDate(invoicePayment.getPaymentDate());
            oldPayment.setMethodOfPayment(invoicePayment.getMethodOfPayment());
            oldPayment.setState(Enumeration.PAYMENT_STATE.VALIDATE);
            oldPayment.setValidateDate(new Date());
            oldPayment.setAccountPaidAfter(invoice.getAmountPaid());
            invoicePaymentRepository.save(oldPayment);

            invoice.setUpdateDate(new Date());
            invoiceRepository.save(invoice);

            return ResponseBody.with(oldPayment, "Paiement enregistre avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody create(InvoicePayment invoicePayment) {
        try {
            String message = "Paiement effectue avec succes";

            Invoice invoice = invoiceRepository.findInvoiceById(invoicePayment.getInvoice().getId());
            if (invoice == null) {
                return ResponseBody.error("Cette facture n'existe pas");
            }

            invoicePayment.setInvoice(invoice);
            invoicePayment.setAmount(invoicePayment.getNetToPay());
            invoicePayment.setBalanceBefore(invoice.getStayToPay());

            double newBalance = invoicePayment.getBalanceBefore() - invoicePayment.getAmount();
            invoicePayment.setBalanceAfter(newBalance);
            invoicePayment.setState(Enumeration.PAYMENT_STATE.PENDING);
            invoicePayment.setCreateDate(new Date());
            invoicePayment.setAccountPaidBefore(invoice.getAmountPaid());
            InvoicePayment newPayment = invoicePaymentRepository.save(invoicePayment);

            // update invoice after payment
            invoice.setStayToPay(newBalance);
            invoice.setAmountPaid(newPayment.getAmount() + invoice.getAmountPaid());
            invoice.setLastPaymentDate(new Date());
            invoice.setUpdateDate(new Date());
            if (invoice.getStayToPay() == 0) {
                invoice.setPaid(true);
                invoice.setState(Enumeration.INVOICE_STATE.PAID);
                message += ", la facture est totalement soldee";
            } else {
                invoice.setState(Enumeration.INVOICE_STATE.IN_PAYMENT);
            }

            // update invoice payment count
            if (invoice.getPaymentCount()==0) {
                // generate payment number
                newPayment.setNumber(invoice.getNumber());
            } else {
                newPayment.setNumber(invoice.getNumber() + "_" + invoice.getPaymentCount());
            }
            invoice.setPaymentCount(invoice.getPaymentCount() + 1);
            invoiceRepository.save(invoice);

            invoicePaymentRepository.save(newPayment);

            return ResponseBody.with(newPayment, message + " !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }


    public ResponseBody save(InvoicePayment invoicePayment) {
        try {

            MESSAGE_OK = "Paiement effectuee avec succes";

            Invoice invoice = invoiceRepository.findInvoiceById(invoicePayment.getInvoice().getId());
            if (invoice == null) {
                return ResponseBody.error("Cette facture n'existe pas");
            }

            invoicePayment.setInvoice(invoice);
            invoicePayment.setBalanceBefore(invoice.getStayToPay());

            double newBalance = invoicePayment.getBalanceBefore() - invoicePayment.getAmount();
            invoicePayment.setBalanceAfter(newBalance);


            InvoicePayment payment = new InvoicePayment(invoicePayment);
            invoicePaymentRepository.save(payment);

            // update invoice after payment
            invoice.setStayToPay(newBalance);
            invoice.setAmountPaid(payment.getAmount() + invoice.getAmountPaid());
            invoice.setLastPaymentDate(new Date());
            invoice.setUpdateDate(new Date());
            if (invoice.getStayToPay() == 0) {
                invoice.setPaid(true);
                invoice.setState(Enumeration.INVOICE_STATE.PAID);
                MESSAGE_OK += ", la facture est totalement soldee";
            } else {
                invoice.setState(Enumeration.INVOICE_STATE.IN_PAYMENT);
            }
            invoiceRepository.save(invoice);

            return ResponseBody.with(payment, MESSAGE_OK + " !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody update(InvoicePayment invoicePayment) {
        try {
            if (!invoicePaymentRepository.findById(invoicePayment.getId()).isPresent()) {
                return ResponseBody.error("Cet Objet n'existe pas !");
            }
            return ResponseBody.with(invoicePaymentRepository.save(invoicePayment), "Paiement modifiee avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }


    public ResponseBody cancelPayment(InvoicePayment invoicePayment) {
        try {

            Invoice invoice = invoiceRepository.findInvoiceById(invoicePayment.getInvoice().getId());
            if (invoice == null) {
                return ResponseBody.error("Cette facture n'existe pas");
            }

            List<InvoicePayment> invoicePayments = invoicePaymentRepository.findAllByIdGreaterThanAndInvoiceIdAndCanceledAndStateEquals(
                    invoicePayment.getId(), invoice.getId(), false, Enumeration.PAYMENT_STATE.PENDING);
            if (invoicePayments != null && !invoicePayments.isEmpty()) {
                return ResponseBody.error("Impossible d'annuler ce paiement\nVous devez d'abord annuler les enregistrements plus recents");
            }

            double newBalance = invoicePayment.getAmount() + invoice.getStayToPay();
            invoice.setStayToPay(newBalance);

            invoice.setAmountPaid(invoice.getAmountPaid() - invoicePayment.getAmount());
            invoice.setPaid(false);

            if (invoice.getAmountPaid() == 0) {
                invoice.setLastPaymentDate(null);
                invoice.setPaymentCount(0);
                invoice.setState(Enumeration.INVOICE_STATE.VALIDATE);
            } else {
                // update payment count
                invoice.setPaymentCount(invoice.getPaymentCount() - 1);
                invoice.setState(Enumeration.INVOICE_STATE.IN_PAYMENT);
            }
            invoiceRepository.save(invoice);


            invoicePaymentRepository.delete(invoicePayment);

            return ResponseBody.success("Paiement annule avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody getById(Long id) {
        try {
            InvoicePayment invoicePayment = invoicePaymentRepository.findInvoicePaymentById(id);
            String amountInLetter = DsnUtils.convertNumberToWord(invoicePayment.getNetToPay()) + " (" + new String(DsnUtils.formatNumber(invoicePayment.getNetToPay())).replaceAll("\\s", ".") + ")".toLowerCase();
            invoicePayment.setAmountInLetter(amountInLetter);
            return ResponseBody.with(invoicePayment, "Paiement recuperee avec succes !");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public List<InvoicePayment> findListByInvoice(Long id) {
        return invoicePaymentRepository.findByInvoiceId(id);
    }

    public ResponseBody getAll() {
        try {
            return ResponseBody.with(invoicePaymentRepository.findAllByCanceled(false), "Toutes les paiements");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }
}
