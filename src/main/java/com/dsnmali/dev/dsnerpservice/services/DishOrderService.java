package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.DishOrderRepository;
import org.springframework.stereotype.Service;

@Service
public class DishOrderService {

    final DishOrderRepository orderDishesRepository;


    public DishOrderService(DishOrderRepository orderDishesRepository) {
        this.orderDishesRepository = orderDishesRepository;
    }
}
