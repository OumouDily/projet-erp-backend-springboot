package com.dsnmali.dev.dsnerpservice.services;


import com.dsnmali.dev.dsnerpservice.entities.Task;
import com.dsnmali.dev.dsnerpservice.respositories.TaskRepository;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.dsnmali.dev.dsnerpservice.utils.ResponseBody;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {
    private final
    TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public ResponseBody save(Task task) {
        try {
            taskRepository.save(task);
            return ResponseBody.with(task, "Task creee avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody edit(Task task) {
        try {
            if (taskRepository.findById(task.getId()).isPresent()) {
                if (task.getState() == null) task.setStartDate(task.getDueDate());
                taskRepository.save(task);
            }
            return ResponseBody.with(task, "Tache modifiee avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody getAll() {
        try {
            List<Task> taches = taskRepository.findAll();
            return ResponseBody.with(taches, taches.size() + "taches");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody find(SearchBody searchBody) {
        try {
            if (searchBody.getAssignToId() != 0) {

                if (searchBody.getCustomerId() != 0 && searchBody.getTaskState() != null) {
                    return ResponseBody.with(taskRepository.findAllByStateAndAssignToIdAndCustomerId(searchBody.getTaskState(),
                            searchBody.getAssignToId(), searchBody.getCustomerId()), "Taches par etat, utilisateur et client/prospect");
                }
                if (searchBody.getCustomerId() != 0 && searchBody.getTaskPriority() != null) {
                    return ResponseBody.with(taskRepository.findAllByPriorityAndAssignToIdAndCustomerId(searchBody.getTaskPriority(),
                            searchBody.getAssignToId(), searchBody.getCustomerId()), "Taches par priorite, utilisateur et client/prospect");
                } else if (searchBody.getCustomerId() != 0) {
                    return ResponseBody.with(taskRepository.findAllByAssignToIdAndCustomerId(searchBody.getAssignToId(), searchBody.getCustomerId()),
                            "Taches par etat, utilisateur et client/prospect");
                } else if (searchBody.getTaskState() != null) {
                    return ResponseBody.with(taskRepository.findAllByStateAndAssignToId(searchBody.getTaskState(),
                            searchBody.getAssignToId()), "Taches de l'utilisateur par etat");
                } else if (searchBody.getTaskPriority() != null) {
                    return ResponseBody.with(taskRepository.findAllByPriorityAndAssignToId(searchBody.getTaskPriority(),
                            searchBody.getAssignToId()), "Taches de l'utilisateur par priorite");
                }
                return ResponseBody.with(taskRepository.findAllByAssignToId(searchBody.getAssignToId()),
                        "Toutes les taches de l'utilisateur");

            } else if (searchBody.getCustomerId() != 0) {
                if (searchBody.getTaskState() != null) {
                    return ResponseBody.with(taskRepository.findAllByStateAndCustomerId(searchBody.getTaskState(),
                            searchBody.getCustomerId()), "Taches du client/prospect par etat");
                } else if (searchBody.getTaskPriority() != null) {
                    return ResponseBody.with(taskRepository.findAllByPriorityAndCustomerId(searchBody.getTaskPriority(),
                            searchBody.getCustomerId()), "Taches du client/prospect par priorite");
                }
                return ResponseBody.with(taskRepository.findAllByCustomerId(searchBody.getCustomerId()), "Toutes les taches par client/prospect");
            } else if (searchBody.getTaskState() != null) {
                return ResponseBody.with(taskRepository.findAllByState(searchBody.getTaskState()), "Toutes les taches par etat");
            } else if (searchBody.getTaskPriority() != null) {
                return ResponseBody.with(taskRepository.findAllByPriority(searchBody.getTaskPriority()), "Toutes les taches par priorite");
            } else {
                return ResponseBody.with(taskRepository.findAll(), "Toutes les taches");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody remove(Long id) {
        try {
            if (taskRepository.findById(id).isPresent()) {
                taskRepository.deleteById(id);
            }
            return ResponseBody.success("Tache supprimee avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est servenue!");
        }
    }

    public ResponseBody getById(Long id) {
        try {
            return ResponseBody.with(taskRepository.findById(id), "Recuperer avec succes");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Erreur");
        }
    }

    public ResponseBody findAll() {
        try {
            List<Task> taches = taskRepository.findAll();
            return ResponseBody.with(taches, taches.size() + " tache.s trouvee.s");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("An error occured");
        }
    }

    public ResponseBody updateByState(Enumeration.TASK_STATE state, Task task) {
        try {
            if (!taskRepository.findById(task.getId()).isPresent()) {
                return ResponseBody.error("Cette tache n'existe pas !");
            }
            Task task1 = taskRepository.findById(task.getId()).get();
            task1.setState(state);
            return ResponseBody.with(taskRepository.save(task1), "Etat de la tache modifie");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }

    public ResponseBody updateByPriority(Enumeration.TASK_PRIORITY priority, Task task) {
        try {
            if (!taskRepository.findById(task.getId()).isPresent()) {
                return ResponseBody.error("Cette tache n'existe pas !");
            }
            Task task1 = taskRepository.findById(task.getId()).get();
            task1.setPriority(priority);
            return ResponseBody.with(taskRepository.save(task1), "Etat de la tache modifie");

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseBody.error("Une erreur est survenue");
        }
    }
}
