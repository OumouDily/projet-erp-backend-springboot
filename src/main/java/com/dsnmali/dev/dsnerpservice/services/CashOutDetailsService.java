package com.dsnmali.dev.dsnerpservice.services;

import com.dsnmali.dev.dsnerpservice.respositories.CashOutDetailsRepository;
import org.springframework.stereotype.Service;

@Service
public class CashOutDetailsService {

    final CashOutDetailsRepository detailCheckoutRepository;

    public CashOutDetailsService(CashOutDetailsRepository detailCheckoutRepository) {
        this.detailCheckoutRepository = detailCheckoutRepository;
    }
}
