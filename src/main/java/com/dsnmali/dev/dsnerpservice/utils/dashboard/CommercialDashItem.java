package com.dsnmali.dev.dsnerpservice.utils.dashboard;

import com.dsnmali.dev.dsnerpservice.entities.User;

import java.io.Serializable;

public class CommercialDashItem implements Serializable {

    private Double achievedTurnover;
    private Double pendingTurnover;
    private Double objectiveTurnover;
    private Double forecastTurnover;
    private User commercial;
    private InvoiceDashBody invoiceDashBody;
    private CrmDashBody crmDashBody;
    private ProjectFolderDashBody projectFolderDashBody;

    public CommercialDashItem() {
    }

    public Double getAchievedTurnover() {
        return achievedTurnover;
    }

    public void setAchievedTurnover(Double achievedTurnover) {
        this.achievedTurnover = achievedTurnover;
    }

    public Double getPendingTurnover() {
        return pendingTurnover;
    }

    public void setPendingTurnover(Double pendingTurnover) {
        this.pendingTurnover = pendingTurnover;
    }

    public Double getObjectiveTurnover() {
        return objectiveTurnover;
    }

    public void setObjectiveTurnover(Double objectiveTurnover) {
        this.objectiveTurnover = objectiveTurnover;
    }

    public Double getForecastTurnover() {
        return forecastTurnover;
    }

    public void setForecastTurnover(Double forecastTurnover) {
        this.forecastTurnover = forecastTurnover;
    }

    public User getCommercial() {
        return commercial;
    }

    public void setCommercial(User commercial) {
        this.commercial = commercial;
    }

    public InvoiceDashBody getInvoiceDashBody() {
        return invoiceDashBody;
    }

    public void setInvoiceDashBody(InvoiceDashBody invoiceDashBody) {
        this.invoiceDashBody = invoiceDashBody;
    }

    public CrmDashBody getCrmDashBody() {
        return crmDashBody;
    }

    public void setCrmDashBody(CrmDashBody crmDashBody) {
        this.crmDashBody = crmDashBody;
    }

    public ProjectFolderDashBody getProjectFolderDashBody() {
        return projectFolderDashBody;
    }

    public void setProjectFolderDashBody(ProjectFolderDashBody projectFolderDashBody) {
        this.projectFolderDashBody = projectFolderDashBody;
    }
}
