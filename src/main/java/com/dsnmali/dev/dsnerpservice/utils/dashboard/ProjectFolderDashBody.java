package com.dsnmali.dev.dsnerpservice.utils.dashboard;

import com.dsnmali.dev.dsnerpservice.entities.ProjectFolder;

import java.io.Serializable;
import java.util.List;

public class ProjectFolderDashBody implements Serializable {

    private int notStartedCount;
    private int inProgressCount;
    private int pendingCount;
    private int doneCount;
    private long sumNotStarted;
    private long sumInProgress;
    private long sumPending;
    private long sumDone;
    private List<ProjectFolder> projectFolders;

    public ProjectFolderDashBody() {
    }

    public int getNotStartedCount() {
        return notStartedCount;
    }

    public void setNotStartedCount(int notStartedCount) {
        this.notStartedCount = notStartedCount;
    }

    public int getInProgressCount() {
        return inProgressCount;
    }

    public void setInProgressCount(int inProgressCount) {
        this.inProgressCount = inProgressCount;
    }

    public int getPendingCount() {
        return pendingCount;
    }

    public void setPendingCount(int pendingCount) {
        this.pendingCount = pendingCount;
    }

    public int getDoneCount() {
        return doneCount;
    }

    public void setDoneCount(int doneCount) {
        this.doneCount = doneCount;
    }

    public long getSumNotStarted() {
        return sumNotStarted;
    }

    public void setSumNotStarted(long sumNotStarted) {
        this.sumNotStarted = sumNotStarted;
    }

    public long getSumInProgress() {
        return sumInProgress;
    }

    public void setSumInProgress(long sumInProgress) {
        this.sumInProgress = sumInProgress;
    }

    public long getSumPending() {
        return sumPending;
    }

    public void setSumPending(long sumPending) {
        this.sumPending = sumPending;
    }

    public long getSumDone() {
        return sumDone;
    }

    public void setSumDone(long sumDone) {
        this.sumDone = sumDone;
    }

    public List<ProjectFolder> getProjectFolders() {
        return projectFolders;
    }

    public void setProjectFolders(List<ProjectFolder> projectFolders) {
        this.projectFolders = projectFolders;
    }
}
