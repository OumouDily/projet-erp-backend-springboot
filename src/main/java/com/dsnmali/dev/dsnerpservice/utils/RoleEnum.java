/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.utils;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author meididiallo
 */
public enum RoleEnum implements GrantedAuthority {

    ADMIN, WEB_ADMIN, MOBILE_ADMIN;

    public String getAuthority() {
        return name();
    }

}
