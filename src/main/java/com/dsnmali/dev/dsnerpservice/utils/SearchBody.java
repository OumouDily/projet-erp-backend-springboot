package com.dsnmali.dev.dsnerpservice.utils;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;

@Data
@ToString
public class SearchBody implements Serializable {
    private long branchId;
    private long userId;
    private long createById;
    private long assignToId;
    private long customerId;
    private long contactId;
    private Optional<Integer> page;
    private Optional<String> sortBy;
    private String year;
    private Date startDate;
    private Date endDate;
    private Enumeration.CUSTOMER_TYPE typeClient;
    private Enumeration.TASK_STATE taskState;
    private Enumeration.TASK_PRIORITY taskPriority;
    private Enumeration.DEVIS_STATE devisState;
    private Enumeration.INVOICE_STATE invoiceState;
}
