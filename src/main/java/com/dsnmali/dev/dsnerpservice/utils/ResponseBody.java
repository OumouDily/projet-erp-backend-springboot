/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.utils;

/**
 * @author meididiallo
 */
public class ResponseBody {

    private String status = null;
    private String message = null;
    private Object response = null;

    public ResponseBody() {
    }

    public ResponseBody(String status, String message, Object response) {
        this.status = status;
        this.message = message;
        this.response = response;
    }

    public ResponseBody(String state, String msg) {
        this.message = msg;
        this.status = state;
    }

    public static ResponseBody with(Object object, String msg) {
        return new ResponseBody("OK", msg, object);
    }

    public static ResponseBody success(String msg) {
        return new ResponseBody("OK", msg);
    }

    public static ResponseBody error(String msg) {
        return new ResponseBody("KO", msg);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

}
