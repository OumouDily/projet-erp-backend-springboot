package com.dsnmali.dev.dsnerpservice.utils;

import java.io.Serializable;

public class EntityConstant implements Serializable {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
