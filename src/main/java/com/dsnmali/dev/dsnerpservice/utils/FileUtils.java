/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author meididiallo
 */
public class FileUtils {

    public static boolean saveFile(String sourceDir, String fileName, byte[] datas) throws IOException {
        File fileDirectory = new File(sourceDir);
        //OutputStream out;
        //InputStream in;
        try {
            if (fileDirectory.isDirectory()) {
                // System.out.println("REP EXISTE");
            } else {
                fileDirectory.mkdirs();
                // System.out.println("REP CREATED");
            }

            try (FileOutputStream fileOutputStream = new FileOutputStream(sourceDir + "/" + fileName)) {
                fileOutputStream.write(datas);
                fileOutputStream.flush();
                return true;
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace(System.err);
        }
        return false;
    }

    public static boolean removeFile(String fileName) throws IOException {
        File fileDirectory = new File(fileName);
        return fileDirectory.delete();
    }
}
