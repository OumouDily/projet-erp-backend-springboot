package com.dsnmali.dev.dsnerpservice.utils.dashboard;

import lombok.Data;
import java.util.List;
@Data
public class InvoiceDashBody {
    private Double sumAmountPaid;
    private Double sumStayToPay;
    private Double totalInvoice;
    private Double achievedTurnover;
    private Double totalDevisOpen;
    private long sumAmountPaidCount;
    private long sumStayToPayCount;
    private long totalInvoiceCount;
    private long totalDevisOpenCount;
    List<GraphBody> invoiceGraphBodies;
}
