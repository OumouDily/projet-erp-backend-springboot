/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.utils.dashboard;

import com.dsnmali.dev.dsnerpservice.entities.Customer;
import com.dsnmali.dev.dsnerpservice.entities.ProjectFolder;

import java.io.Serializable;
import java.util.List;

/**
 * @author meididiallo
 */
public class ProjectDashBody implements Serializable {

    private int totalProspectCount;
    private int totalCustomerCount;
    private int newProspectCount;
    private int notContactedProspectCount;
    private List<Customer> customers;
    private List<GraphBody> customerGraphBodies;

    private int totalProjectCount;
    private int notStatedProjectCount;
    private int statedProjectCount;
    private int doneProjectCount;
    private int pendingProjectCount;
    private int cancelProjectCount;
    private List<ProjectFolder> projects;
    private List<GraphBody> projectGraphBodies;


    public ProjectDashBody() {
    }

    public int getTotalProspectCount() {
        return totalProspectCount;
    }

    public void setTotalProspectCount(int totalProspectCount) {
        this.totalProspectCount = totalProspectCount;
    }

    public int getNewProspectCount() {
        return newProspectCount;
    }

    public void setNewProspectCount(int newProspectCount) {
        this.newProspectCount = newProspectCount;
    }

    public int getNotContactedProspectCount() {
        return notContactedProspectCount;
    }

    public void setNotContactedProspectCount(int notContactedProspectCount) {
        this.notContactedProspectCount = notContactedProspectCount;
    }

    public int getTotalCustomerCount() {
        return totalCustomerCount;
    }

    public void setTotalCustomerCount(int totalCustomerCount) {
        this.totalCustomerCount = totalCustomerCount;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public List<GraphBody> getCustomerGraphBodies() {
        return customerGraphBodies;
    }

    public void setCustomerGraphBodies(List<GraphBody> customerGraphBodies) {
        this.customerGraphBodies = customerGraphBodies;
    }
}
