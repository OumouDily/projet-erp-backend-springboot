/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.utils;

/**
 * @author meididiallo
 */
public class Enumeration {

    public enum INVOICE_TYPE {
        DEVIS("DEVIS"),
        FACT("FACT");

        private final String desc;

        INVOICE_TYPE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum STATE {
        PENDING("En attente"),
        OPEN("Ouvert"),
        IN_PAYMENT("En paiement"),
        CLOSE("Ferme"),
        CANCEL("Annule"),
        VALID("Valide"),
        DELIVER("Livre");

        private final String desc;

        STATE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum DEVIS_STATE {
        OPEN("Ouvert"),
        INVOICED("Facture"),
        VALIDATE("Valide"),
        DUPLICATE("Duplique"),
        CANCEL("Annule");

        private final String desc;

        DEVIS_STATE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum INVOICE_STATE {
        OPEN("Ouverte"),
        VALIDATE("Validee"),
        IN_PAYMENT("En paiement"),
        PAID("Payee"),
        CANCEL("Annulee");

        private final String desc;

        INVOICE_STATE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum PAYMENT_STATE {
        PENDING("En attente"),
        VALIDATE("Payee");

        private final String desc;

        PAYMENT_STATE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum CUSTOMER_STATE {
        NOT_CONTACTED("Non contacte"),
        CONTACTED("Contacte"),
        PREQUALIFIED("Prequalifie"),
        QUALIFIED("Qualifie"),
        LOSED("Perdu");

        private final String desc;

        CUSTOMER_STATE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum CUSTOMER_TYPE {
        PROSPECT("PROSPECT"),
        CLIENT("CLIENT");

        private final String desc;

        CUSTOMER_TYPE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum TASK_STATE {
        TODO("Non commence"),
        IN_PROGRESS("Commence"),
        DONE("Termine"),
        PENDING("En attente");

        private final String desc;

        TASK_STATE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum PROJECT_FOLDER_STATE {
        TODO("Non commence"),
        IN_PROGRESS("Commence"),
        DONE("Termine"),
        PENDING("En attente"),
        CANCEL("Annulee");

        private final String desc;

        PROJECT_FOLDER_STATE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum TASK_ACTION {
        PHONING("Phoning"),
        RDV("Rendez-vous"),
        MAILING("Mailing"),
        NEGO("Negociation"),
        OTHER("Autre");

        private final String title;

        TASK_ACTION(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }
    }

    public enum TASK_PRIORITY {
        LOW("Faible"),
        MEDIUM("Moyenne"),
        HIGH("Haute"),
        URGENT("Urgente");

        private final String desc;

        TASK_PRIORITY(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum PROJECT_FOLDER_PRIORITY {
        LOW("Faible"),
        MEDIUM("Moyenne"),
        HIGH("Haute"),
        URGENT("Urgente");

        private final String desc;

        PROJECT_FOLDER_PRIORITY(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum METHOD_OF_PAYMENT {
        CHECK("Chèque"),
        TRANSFER("Virement"),
        CASH("Espèce");

        private final String desc;

        METHOD_OF_PAYMENT(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    public enum CASH_DESK_TYPE {
        PROCUREMENT("Procurement");

        private final String desc;

        CASH_DESK_TYPE(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

}
