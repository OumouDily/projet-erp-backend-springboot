/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * @author meididiallo
 */
public class ImageUtils {

    public static boolean saveImage(String imageDir, String saveFileDir, byte[] datas) throws IOException {
        File fileDirectory = new File(imageDir);
        //OutputStream out;
        //InputStream in;
        try {
            if (fileDirectory.isDirectory()) {
                // System.out.println("REP EXISTE");
            } else {
                fileDirectory.mkdirs();
                // System.out.println("REP CREATED");
            }
            /*in = file.getInputstream();
            out = new FileOutputStream(new File(saveFile));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();*/
            try (FileOutputStream fileOutputStream = new FileOutputStream(saveFileDir)) {
                //fileOutputStream.write(resizePrincipaleImage(datas, "png", 450, 500));
                fileOutputStream.write(datas);
                fileOutputStream.flush();
                return true;
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace(System.err);
        }
        return false;
    }

    /**
     * Resizes an image to a absolute width and height (the image may not be
     * proportional)
     *
     * @param datas        Path of the original image
     * @param format
     * @param scaledWidth  absolute width in pixels
     * @param scaledHeight absolute height in pixels
     * @return
     * @throws IOException
     */
    public static byte[] resizePrincipaleImage(byte[] datas,
                                               String format, int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        //File inputFile = new File(inputImagePath);
        //BufferedImage inputImage = ImageIO.read(inputFile);
        ByteArrayInputStream arrayInputStream = new ByteArrayInputStream(datas);
        BufferedImage inputImage = ImageIO.read(arrayInputStream);

        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();

        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        // extracts extension of output file
        String formatName = format;

        // writes to output file
        ImageIO.write(outputImage, formatName, arrayOutputStream);
        return arrayOutputStream.toByteArray();
    }

    /**
     * Resizes an image to a absolute width and height (the image may not be
     * proportional)
     *
     * @param inputImagePath  Path of the original image
     * @param outputImagePath Path to save the resized image
     * @param scaledWidth     absolute width in pixels
     * @param scaledHeight    absolute height in pixels
     * @throws IOException
     */
    public static void resize(String inputImagePath,
                              String outputImagePath, int scaledWidth, int scaledHeight)
            throws IOException {
        // reads input image
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);

        // creates output image
        BufferedImage outputImage = new BufferedImage(scaledWidth,
                scaledHeight, inputImage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
        g2d.dispose();

        // extracts extension of output file
        String formatName = outputImagePath.substring(outputImagePath
                .lastIndexOf(".") + 1);

        // writes to output file
        ImageIO.write(outputImage, formatName, new File(outputImagePath));
    }

    /**
     * Resizes an image by a percentage of original size (proportional).
     *
     * @param inputImagePath  Path of the original image
     * @param outputImagePath Path to save the resized image
     * @param percent         a double number specifies percentage of the output image
     *                        over the input image.
     * @throws IOException
     */
    public static void resize(String inputImagePath,
                              String outputImagePath, double percent) throws IOException {
        File inputFile = new File(inputImagePath);
        BufferedImage inputImage = ImageIO.read(inputFile);
        int scaledWidth = (int) (inputImage.getWidth() * percent);
        int scaledHeight = (int) (inputImage.getHeight() * percent);
        resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
    }

}
