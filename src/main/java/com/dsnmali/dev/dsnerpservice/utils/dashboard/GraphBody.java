/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.utils.dashboard;

import com.dsnmali.dev.dsnerpservice.entities.Customer;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author meididiallo
 */
@Data
public class GraphBody implements Serializable {

    private String name;
    private Double value;

    public GraphBody(String name, Double value) {
        this.name = name;
        this.value = value;
    }
}
