package com.dsnmali.dev.dsnerpservice.utils.dashboard;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

public class CommercialDashBody implements Serializable {

    private Double achievedTurnover;
    private Double pendingTurnover;
    private Double objectiveTurnover;
    private Double forecastTurnover;
    private Set<CommercialDashItem> commercialDashItems;

    public CommercialDashBody() {
    }


    public Double getAchievedTurnover() {
        return achievedTurnover;
    }

    public void setAchievedTurnover(Double achievedTurnover) {
        this.achievedTurnover = achievedTurnover;
    }

    public Double getPendingTurnover() {
        return pendingTurnover;
    }

    public void setPendingTurnover(Double pendingTurnover) {
        this.pendingTurnover = pendingTurnover;
    }

    public Double getObjectiveTurnover() {
        return objectiveTurnover;
    }

    public void setObjectiveTurnover(Double objectiveTurnover) {
        this.objectiveTurnover = objectiveTurnover;
    }

    public Double getForecastTurnover() {
        return forecastTurnover;
    }

    public void setForecastTurnover(Double forecastTurnover) {
        this.forecastTurnover = forecastTurnover;
    }

    public Set<CommercialDashItem> getCommercialDashItems() {
        return commercialDashItems;
    }

    public void setCommercialDashItems(Set<CommercialDashItem> commercialDashItems) {
        this.commercialDashItems = commercialDashItems;
    }
}
