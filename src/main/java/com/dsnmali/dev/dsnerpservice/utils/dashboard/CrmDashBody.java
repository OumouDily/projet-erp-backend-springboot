/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.utils.dashboard;

import com.dsnmali.dev.dsnerpservice.entities.Customer;

import java.io.Serializable;
import java.util.List;

/**
 * @author meididiallo
 */
public class CrmDashBody implements Serializable {

    private int prospectCount;
    private int customerCount;
    private int newProspectCount;
    private int notContactedProspectCount;
    private List<Customer> customers;
    private List<GraphBody> customerGraphBodies;

    public CrmDashBody() {
    }

    public int getProspectCount() {
        return prospectCount;
    }

    public void setProspectCount(int prospectCount) {
        this.prospectCount = prospectCount;
    }

    public int getNewProspectCount() {
        return newProspectCount;
    }

    public void setNewProspectCount(int newProspectCount) {
        this.newProspectCount = newProspectCount;
    }

    public int getNotContactedProspectCount() {
        return notContactedProspectCount;
    }

    public void setNotContactedProspectCount(int notContactedProspectCount) {
        this.notContactedProspectCount = notContactedProspectCount;
    }

    public int getCustomerCount() {
        return customerCount;
    }

    public void setCustomerCount(int customerCount) {
        this.customerCount = customerCount;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public List<GraphBody> getCustomerGraphBodies() {
        return customerGraphBodies;
    }

    public void setCustomerGraphBodies(List<GraphBody> customerGraphBodies) {
        this.customerGraphBodies = customerGraphBodies;
    }
}
