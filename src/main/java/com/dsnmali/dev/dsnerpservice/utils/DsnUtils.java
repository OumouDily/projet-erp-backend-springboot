/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsnmali.dev.dsnerpservice.utils;

import com.dsnmali.dev.dsnerpservice.entities.User;
import com.dsnmali.dev.dsnerpservice.utils.dashboard.GraphBody;
import pl.allegro.finance.tradukisto.MoneyConverters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author meididiallo
 */
public class DsnUtils {

    private static Calendar calendar = Calendar.getInstance();
    private static int getValue;

    public static Date addMonthToDate(Date date, int add) {
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, add);
        return calendar.getTime();
    }

    public static Date subMonthToDate(Date date, int sub) {
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, -sub);
        return calendar.getTime();
    }

    public static Date addDayToDate(Date date, int add) {
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, add);
        return calendar.getTime();
    }

    public static Date subDayToDate(Date date, int sub) {
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -sub);
        return calendar.getTime();
    }

    public static Date toDate(String dateSource, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).parse(dateSource);
    }

    public static String toString(Date date, String pattern) throws ParseException {
        return new SimpleDateFormat(pattern).format(date);
    }

    public static String toString(Date date, String pattern, Locale locale) throws ParseException {
        return new SimpleDateFormat(pattern, locale).format(date);
    }


    public static long getNombreJours(Date dateDebut, Date dateFin) {
        long diff = dateFin.getTime() - dateDebut.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + 1; // +1 = dernière date inclus
    }

    public static int getNombreAnnee(Date pastDate) {
        Calendar present = Calendar.getInstance();
        Calendar past = Calendar.getInstance();
        past.setTime(pastDate);

        int years = 0;

        while (past.before(present)) {
            past.add(Calendar.YEAR, 1);
            if (past.before(present)) {
                years++;
            }
        }
        return years;
    }

    public static String getTimes(Date dateDebut, Date dateFin) {
        String ret = "";
        long diff = dateFin.getTime() - dateDebut.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);
        int diffInDays = (int) ((dateFin.getTime() - dateDebut.getTime()) / (1000 * 60 * 60 * 24));

        if (diffInDays > 1 || diffHours > 24) {
            ret += diffInDays + " jour(s). ";
            diffHours = diffHours - (diffInDays * 24);
        }

        if (diffHours != 24 && diffHours > 0) {
            ret += diffHours + " h. ";
        }

        if (diffMinutes > 0) {
            ret += diffMinutes + " mn. ";
        }

        if (diffSeconds > 0) {
            ret += diffSeconds + " s.";
        }
        return ret;
    }

    public static Date getFirstDayOfTheMonth(Date date, String dateFormat) {
        SimpleDateFormat sfd = new SimpleDateFormat(dateFormat);
        Date dayOfMonth = new Date();
        try {
            String format = sfd.format(date);
            Calendar mCalendar = Calendar.getInstance();
            mCalendar.setTime(sfd.parse(format));
            mCalendar.setTime(date);

            //calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            mCalendar.set(Calendar.DAY_OF_MONTH, mCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            dayOfMonth = mCalendar.getTime();

        } catch (ParseException ex) {
            Logger.getLogger(Date.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dayOfMonth;
    }

    public static Date getLastDayOfTheMonth(Date date, String dateFormat) {
        SimpleDateFormat sfd = new SimpleDateFormat(dateFormat);
        Date dayOfMonth = new Date();
        try {
            String format = sfd.format(date);
            Calendar mCalendar = Calendar.getInstance();
            mCalendar.setTime(sfd.parse(format));

            //calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
            mCalendar.set(Calendar.DAY_OF_MONTH, mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            dayOfMonth = mCalendar.getTime();

        } catch (ParseException ex) {
            Logger.getLogger(Date.class.getName()).log(Level.SEVERE, null, ex);
        }

        return dayOfMonth;
    }

    public static String formatNumber(double number) {
        return NumberFormat.getNumberInstance(Locale.FRENCH).format(number);
    }

    public static String formatNumber(long number) {
        return NumberFormat.getNumberInstance(Locale.FRENCH).format(number);
    }

    public static String getHtml(String uri) throws IOException {
        URL url = new URL(uri);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(url.openStream()));

        StringBuilder ouputLine = new StringBuilder();
        String inputLine;
        while ((inputLine = in.readLine()) != null)
            ouputLine.append(inputLine);
        in.close();
        return String.valueOf(ouputLine);
    }


    public static char[] getFrenchAlphabets() {
        final char[] alphabet = {
                'a', 'b', 'c', 'd', 'e', 'f',
                'g', 'h', 'i', 'j', 'k', 'l',
                'm', 'n', 'o', 'p', 'q', 'r',
                's', 't', 'u', 'v', 'w', 'x',
                'y', 'z'
        };
        return alphabet;
    }

    public static String generateStringNumber(long id) {
        String number;
        if (id < 10) {
            number = "00" + id + "";
        } else if (id < 100) {
            number = "0" + id + "";
        } else {
            number = id + "";
        }

        return number;
    }

    public static String textToLowerCase(String text) {
        return text.toLowerCase();
    }

    public static String ajusteMontant(String montantEnLettre) {
        System.out.println(montantEnLettre);
        String[] n1000 = {"Mille", "Million", "Milliard", "Billion", "Billiard"};
        List<String> lettres = Arrays.asList(n1000);
        String[] bits = montantEnLettre.split(" ");
        String lastOne = bits[bits.length - 1];
        String beforeLastOne = bits[bits.length - 2];
        if (!lettres.contains(beforeLastOne) && !beforeLastOne.equalsIgnoreCase("Un") && lastOne.equalsIgnoreCase("Cent")) {
            montantEnLettre += "s";
        }
        return montantEnLettre;
    }

    public static String getAmountInLetter(Double nombre, String devise) {
        return Convert.FR(String.valueOf(nombre)).replace("et Zero", "").trim() + " " + devise;
        // return ajusteMontant(Convert.FR(String.valueOf(nombre == null ? 0 : nombre)).replace("et Zero", "").trim()) + " " + devise;
    }

    public static String convertNumberToWord(Double number) {
        MoneyConverters converter = MoneyConverters.FRENCH_BANKING_MONEY_VALUE;
        return converter.asWords(new BigDecimal(number)).replace(" € 00/100", "");
    }

    public static String getInitialFromUserName(User user) {
        StringBuilder ret = new StringBuilder();
        String[] nomTab = user.getLastname().trim().split(" ");
        String[] firstnameTab = user.getFirstname().trim().split(" ");
        if (nomTab.length > 1) {
            for (String val : nomTab) {
                ret.append(val, 0, 1);
            }
        } else {
            ret.append(nomTab[0], 0, 1);
        }
        if (firstnameTab.length > 1) {
            for (String val : firstnameTab) {
                ret.append(val, 0, 1);
            }
        } else {
            ret.append(firstnameTab[0], 0, 1);
        }

        return ret.toString().toUpperCase();
    }

    public static long getMontantTva(long prixHt, int tva) {
        // System.out.println("(1 + "+tva+" / 100) = " + ((1 + (tva * 0.01))));
        return (long) ((1 + (tva * 0.01)) * prixHt);
    }

    public static double getMontantTva(double prixHt, int tva) {
        // System.out.println("(1 + "+tva+" / 100) = " + ((1 + (tva * 0.01))));
        return (long) ((1 + (tva * 0.01)) * prixHt);
    }

    public static String generateECNumber(User user) {
        return "DSN-EC." + new SimpleDateFormat("ddMMyyyy'.'HHmmss").format(new Date()) + "." + getInitialFromUserName(user);
    }

    public static String generateSCNumber(User user) {
        return "DSN-SC." + new SimpleDateFormat("ddMMyyyy'.'HHmmss").format(new Date()) + "." + getInitialFromUserName(user);
    }

    public String generateNumber(String text, Date date) {
        getValue++;
        return text + "-" + new SimpleDateFormat("MMYYY").format(date) + "-" + "00" + getValue;
    }

    public static List<GraphBody> getGraphBodies(HashMap<String, Double> hashMap) {
        List<GraphBody> ret = new ArrayList<>();
        for (Map.Entry<String, Double> entry : hashMap.entrySet()) {
            ret.add(new GraphBody(entry.getKey(), entry.getValue()));
        }
        return ret;
    }

    public static List<Date> getLast12Months() {
        List<Date> ret = new LinkedList<>();
        try {
            Date today = new Date();
            String maxDate = toString(today, "MM-yyyy");
            Calendar cal = Calendar.getInstance();
            cal.setTime(toDate(maxDate, "MM-yyyy"));
            for (int i = 1; i <= 12; i++) {
                ret.add(cal.getTime());

                cal.add(Calendar.MONTH, -1);
            }
            Collections.reverse(ret);
        } catch (ParseException e) {
            e.printStackTrace(System.err);
        }
        return ret;
    }

    public static Date getYearDate(String year) {
        Date ret = new Date();
        try {
            String date = year + "-01-01";
            ret = toDate(date, "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
