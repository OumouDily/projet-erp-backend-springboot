package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.ProductNature;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductNatureRepository extends JpaRepository<ProductNature, Long> {
}
