package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.CashOut;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CashOutRepository extends JpaRepository<CashOut, Long> {
}
