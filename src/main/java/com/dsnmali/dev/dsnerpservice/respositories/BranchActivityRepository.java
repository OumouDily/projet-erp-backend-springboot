package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.BranchActivity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface BranchActivityRepository extends JpaRepository<BranchActivity, Long> {

    BranchActivity findByName(@Param("name") String name);
}
