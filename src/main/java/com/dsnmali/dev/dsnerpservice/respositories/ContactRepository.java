package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.BranchActivity;
import com.dsnmali.dev.dsnerpservice.entities.Contact;
import com.dsnmali.dev.dsnerpservice.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface ContactRepository extends JpaRepository<Contact, Long> {

    Contact findContactById(@Param("id") Long id);

    @Query("select c from Contact c, CustomerContact cc where cc.contact=c and cc.customer.id=:customerId")
    Set<Contact> findAllByCustomerId(@Param("customerId") Long customerId);

}
