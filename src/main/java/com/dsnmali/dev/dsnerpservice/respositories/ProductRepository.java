package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Contact;
import com.dsnmali.dev.dsnerpservice.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface ProductRepository extends JpaRepository<Product, Long> {

    Product findProductById(@Param("id") Long id);

    @Query("select p from Product p order by p.productCategory.name, p.designation")
    Set<Product> findAllProducts();

    @Query("select p from Product p where p.productCategory.id=:categoryId order by p.productCategory.name, p.designation")
    Set<Product> findAllByProductCategoryId(@Param("categoryId") Long categoryId);

}
