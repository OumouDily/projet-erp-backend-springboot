package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Procurement;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author meididiallo
 */
public interface ProcurementRepository extends JpaRepository<Procurement, Long> {
}
