package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.CustomerContact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface CustomerContactRepository extends JpaRepository<CustomerContact, Long> {

    Set<CustomerContact> findAllByContactId(@Param("contactId") Long contactId);

    Set<CustomerContact> findAllByCustomerId(@Param("customerId") Long customerId);
}
