package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Devis;
import com.dsnmali.dev.dsnerpservice.entities.Invoice;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    Invoice findInvoiceById(@Param("id") Long id);

    @Query("SELECT invoice FROM Invoice invoice WHERE invoice.devis.id=:id")
    Invoice findByDevis(@Param(value = "id") Long id);

    Page<Invoice> findAllInvoiceByEnabled(boolean enabled, Pageable pageable);

    Page<Invoice> findAllInvoiceByEnabledAndCustomerId(boolean enabled, Pageable pageable,
                                                       @Param("customerId") Long customerId);

    Page<Invoice> findAllInvoiceByEnabledAndAgentId(boolean enabled, Pageable pageable,
                                                    @Param("agentId") Long agentId);

    Page<Invoice> findAllInvoiceByEnabledAndState(boolean enabled, Pageable pageable,
                                                  @Param("state") Enumeration.INVOICE_STATE state);

    Page<Invoice> findAllInvoiceByEnabledAndCustomerIdAndState(boolean enabled, Pageable pageable,
                                                               @Param("customerId") Long customerId,
                                                               @Param("state") Enumeration.INVOICE_STATE state);

    Page<Invoice> findAllInvoiceByEnabledAndAgentIdAndState(boolean enabled, Pageable pageable,
                                                            @Param("agentId") Long agentId,
                                                            @Param("state") Enumeration.INVOICE_STATE state);

    Page<Invoice> findAllInvoiceByEnabledAndCustomerIdAndAgentId(boolean enabled, Pageable pageable,
                                                                 @Param("agentId") Long agentId,
                                                                 @Param("customerId") Long customerId);

    Page<Invoice> findAllInvoiceByEnabledAndCustomerIdAndAgentIdAndState(boolean enabled, Pageable pageable,
                                                                         @Param("agentId") Long agentId,
                                                                         @Param("customerId") Long customerId,
                                                                         @Param("state") Enumeration.INVOICE_STATE state);

    List<Invoice> findAllInvoiceByEnabled(@Param("enabled") boolean enabled);

    List<Invoice> findAllInvoiceByEnabledAndCustomerId(boolean enabled,
                                                       @Param("customerId") Long customerId);

    List<Invoice> findAllInvoiceByEnabledAndAgentId(boolean enabled,
                                                    @Param("agentId") Long agentId);

    List<Invoice> findAllInvoiceByEnabledAndState(boolean enabled,
                                                  @Param("state") Enumeration.INVOICE_STATE state);

    List<Invoice> findAllInvoiceByEnabledAndCustomerIdAndState(boolean enabled,
                                                               @Param("customerId") Long customerId,
                                                               @Param("state") Enumeration.INVOICE_STATE state);

    List<Invoice> findAllInvoiceByEnabledAndAgentIdAndState(boolean enabled,
                                                            @Param("agentId") Long agentId,
                                                            @Param("state") Enumeration.INVOICE_STATE state);

    List<Invoice> findAllInvoiceByEnabledAndCustomerIdAndAgentId(boolean enabled,
                                                                 @Param("agentId") Long agentId,
                                                                 @Param("customerId") Long customerId);

    List<Invoice> findAllInvoiceByEnabledAndCustomerIdAndAgentIdAndState(boolean enabled,
                                                                         @Param("agentId") Long agentId,
                                                                         @Param("customerId") Long customerId,
                                                                         @Param("state") Enumeration.INVOICE_STATE state);

    List<Invoice> findAllInvoiceByPaid(@Param("paid") boolean paid);

    List<Invoice> findAllByStateIsNot(@Param(value = "state") Enumeration.INVOICE_STATE state);

    @Query("SELECT i from Invoice i where DATE_FORMAT(i.invoiceDate, '%Y') = DATE_FORMAT(:year, '%Y') " +
            "and i.state<>:state and i.agent.id=:agentId and i.enabled=true")
    List<Invoice> findAllByStateIsNotAndYear(@Param(value = "state") Enumeration.INVOICE_STATE state,
                                             @Param("year") Date year);

    @Query("SELECT i from Invoice i where i.state<>:state and i.agent.id=:agentId and i.enabled=true and " +
            "(DATE_FORMAT(i.invoiceDate, '%Y%m%d') between DATE_FORMAT(:startDate, '%Y%m%d') and DATE_FORMAT(:endDate, '%Y%m%d'))")
    List<Invoice> findAllByStateIsNotAndDates(@Param(value = "state") Enumeration.INVOICE_STATE state,
                                              @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    List<Invoice> findAllByStateIsNotAndAgentId(@Param(value = "state") Enumeration.INVOICE_STATE state,
                                                @Param("agentId") long agentId);

    @Query("SELECT i from Invoice i where DATE_FORMAT(i.invoiceDate, '%Y') = DATE_FORMAT(:year, '%Y') " +
            "and i.state<>:state and i.agent.id=:agentId and i.enabled=true")
    List<Invoice> findAllByStateIsNotAndAgentIdAndYear(@Param("state") Enumeration.INVOICE_STATE state,
                                                       @Param("agentId") long agentId,
                                                       @Param("year") Date year);

    @Query("SELECT i from Invoice i where i.state<>:state and i.agent.id=:agentId and i.enabled=true and " +
            "(DATE_FORMAT(i.invoiceDate, '%Y%m%d') between DATE_FORMAT(:startDate, '%Y%m%d') and DATE_FORMAT(:endDate, '%Y%m%d'))")
    List<Invoice> findAllByStateIsNotAndAgentIdAndDates(@Param("state") Enumeration.INVOICE_STATE state,
                                                        @Param("agentId") long agentId,
                                                        @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    boolean existsByNumber(String number);

    boolean existsByDevisIdAndEnabled(Long devisId, boolean enabled);

    List<Invoice> findAllByPaid(boolean paid);

    List<Invoice> findAllByPaidAndEditable(boolean paid, boolean editable);

    List<Invoice> findAllByAgentId(@Param("agentId") Long agentId);

    List<Invoice> findAllByCustomerId(@Param("customerId") Long customerId);

    List<Invoice> findAllByCustomerIdAndAgentId(@Param("customerId") Long customerId, @Param("agentId") Long agentId);

    @Query("SELECT sum(i.total) from Invoice i where i.customer.id=:customerId and i.state<>:state and i.enabled=true")
    Number findAllState(@Param("customerId") Long customerId, @Param("state") Enumeration.INVOICE_STATE state);

    @Query("SELECT sum(i.total) from Invoice i where i.customer.id=:customerId and i.state<>:state and i.enabled=true")
    Double sumTotalByCustomerIdAndStateNot(@Param("customerId") Long customerId, @Param("state") Enumeration.INVOICE_STATE state);

    @Query("SELECT sum(i.total) from Invoice i where DATE_FORMAT(i.invoiceDate, '%Y%m') = DATE_FORMAT(:invoiceDate, '%Y%m') and " +
            "i.state<>:state and i.enabled=true")
    Double sumTotalByInvoiceDateAndStateNot(@Param("invoiceDate") Date invoiceDate, @Param("state") Enumeration.INVOICE_STATE state);

    @Query("SELECT sum(i.total) from Invoice i where DATE_FORMAT(i.invoiceDate, '%Y') = DATE_FORMAT(:year, '%Y') and " +
            "i.state<>:state and i.enabled=true")
    Double sumTotalByStateNotAndYear(@Param("year") Date year, @Param("state") Enumeration.INVOICE_STATE state);

    @Query("SELECT sum(i.total) from Invoice i where i.state<>:state and i.enabled=true and " +
            "(DATE_FORMAT(i.invoiceDate, '%Y%m%d') between DATE_FORMAT(:startDate, '%Y%m%d') and DATE_FORMAT(:endDate, '%Y%m%d'))")
    Double sumTotalByStateNotAndDates(@Param("state") Enumeration.INVOICE_STATE state,
                                      @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query("SELECT sum(i.total) from Invoice i where DATE_FORMAT(i.invoiceDate, '%Y%m') = DATE_FORMAT(:invoiceDate, '%Y%m') and i.state<>:state and i.agent.id=:agentId and i.enabled=true")
    Double sumTotalByInvoiceDateAndStateNotAndAgentId(@Param("invoiceDate") Date invoiceDate,
                                                      @Param("state") Enumeration.INVOICE_STATE state,
                                                      @Param("agentId") Long agentId);

    @Query("SELECT sum(i.total) from Invoice i where DATE_FORMAT(i.invoiceDate, '%Y') = DATE_FORMAT(:year, '%Y') and " +
            "i.state<>:state and i.agent.id=:agentId and i.enabled=true")
    Double sumTotalByStateNotAndAgentIdAndYear(@Param("year") Date year,
                                               @Param("state") Enumeration.INVOICE_STATE state,
                                               @Param("agentId") Long agentId);

    @Query("SELECT sum(i.total) from Invoice i where i.state<>:state and i.agent.id=:agentId and i.enabled=true and " +
            "(DATE_FORMAT(i.invoiceDate, '%Y%m%d') between DATE_FORMAT(:startDate, '%Y%m%d') and DATE_FORMAT(:endDate, '%Y%m%d'))")
    Double sumTotalByStateNotAndAgentIdAndDates(@Param("state") Enumeration.INVOICE_STATE state,
                                                @Param("agentId") Long agentId,
                                                @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
