package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface ParameterRepository extends JpaRepository<Parameter, Long> {

    Parameter findParameterById(@Param(value = "id") Long id);

}
