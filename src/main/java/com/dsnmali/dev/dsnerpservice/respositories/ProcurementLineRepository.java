package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.ProcurementLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcurementLineRepository extends JpaRepository<ProcurementLine, Long> {
}
