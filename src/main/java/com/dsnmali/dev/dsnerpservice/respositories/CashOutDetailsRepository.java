package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.CashOutDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CashOutDetailsRepository extends JpaRepository<CashOutDetails, Long> {
}
