package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.DishOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishOrderRepository extends JpaRepository<DishOrder, Long> {
}
