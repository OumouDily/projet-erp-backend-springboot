package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Product;
import com.dsnmali.dev.dsnerpservice.entities.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {

    ProductCategory findProductCategoriesById(@Param("id") Long id);

    @Override
    @Query("select pc from ProductCategory pc order by pc.name")
    List<ProductCategory> findAll();
}
