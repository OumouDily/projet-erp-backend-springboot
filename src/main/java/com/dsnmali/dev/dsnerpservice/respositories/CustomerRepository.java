package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Customer;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    @Query("SELECT pc FROM Customer pc WHERE pc.type='PROSPECT'")
    Page<Customer> getAllProspect(Pageable pageable, @Param("agentId") Long agentId);

    @Query("SELECT pc FROM Customer pc WHERE pc.type='CLIENT'")
    Page<Customer> getAllCustomer(Pageable pageable);

    @Query("SELECT pc FROM Customer pc WHERE pc.type=:type")
    Page<Customer> getCustomers(Pageable pageable, @Param("type") Enumeration.CUSTOMER_TYPE type);

    @Query("SELECT pc FROM Customer pc WHERE pc.type=:type and pc.agent.id=:agentId")
    Page<Customer> getCustomers(Pageable pageable, @Param("agentId") Long agentId,
                                @Param("type") Enumeration.CUSTOMER_TYPE type);

    @Query("SELECT pc FROM Customer pc WHERE pc.agent.id=:agentId")
    List<Customer> getCustomers(@Param("agentId") Long agentId);

    @Query("SELECT pc FROM Customer pc WHERE pc.type=:type and pc.agent.id=:agentId")
    List<Customer> getCustomers(@Param("agentId") Long agentId, @Param("type") Enumeration.CUSTOMER_TYPE type);

    @Query("SELECT pc FROM Customer pc WHERE pc.type=:type")
    List<Customer> getCustomers(@Param("type") Enumeration.CUSTOMER_TYPE type);

    @Query("SELECT pc FROM Customer pc WHERE pc.branchActivity.id=:branchId AND pc.type=:type")
    List<Customer> findAllProspectClientBySector(@Param(value = "branchId") Long branchId,
                                                 @Param(value = "type") Enumeration.CUSTOMER_TYPE type);

    @Query("SELECT pc FROM Customer pc WHERE pc.type=:type")
    List<Customer> findAllProspectClientByType(@Param(value = "type") Enumeration.CUSTOMER_TYPE type);

    @Query("SELECT pc FROM Customer pc WHERE pc.type='PROSPECT'")
    List<Customer> findAllProspect();

    @Query("SELECT pc FROM Customer pc WHERE pc.type='CLIENT'")
    List<Customer> findAllClient();

    @Query("SELECT pc FROM Customer pc WHERE pc.agent.id=:agentId AND pc.type=:type")
    List<Customer> findAllProspectClientByAgentId(@Param(value = "agentId") Long agentId,
                                                  @Param(value = "type") Enumeration.CUSTOMER_TYPE type);

    @Query("SELECT pc FROM Customer pc WHERE pc.branchActivity.id=:branchId AND pc.agent.id=:agentId AND pc.type=:type")
    List<Customer> findAllProspectBySectorAndAgentId(@Param(value = "branchId") Long branchId,
                                                     @Param(value = "agentId") Long agentId,
                                                     @Param(value = "type") Enumeration.CUSTOMER_TYPE type
    );

    boolean existsByName(String name);

    @Query("SELECT pc FROM Customer pc WHERE pc.id <> :id AND pc.name = :name")
    List<Customer> distinctByIdAndExistByName(@Param(value = "id") Long id, @Param(value = "name") String name);

    @Query("select c from Customer c, CustomerContact cc where cc.customer=c and cc.contact.id=:contactId")
    Set<Customer> findAllByContactId(@Param("contactId") Long contactId);



}
