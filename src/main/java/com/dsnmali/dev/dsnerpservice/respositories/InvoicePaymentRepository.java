package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.InvoicePayment;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InvoicePaymentRepository extends JpaRepository<InvoicePayment, Long> {

    @Query("SELECT payment FROM InvoicePayment payment WHERE payment.invoice.id=:id")
    List<InvoicePayment> findByInvoiceId(@Param(value = "id") Long id);

    InvoicePayment findInvoicePaymentById(@Param(value = "id") Long id);

    List<InvoicePayment> findAllByCanceled(boolean canceled);

    List<InvoicePayment> findAllByIdLessThanAndInvoiceIdAndCanceledAndStateEquals(@Param("id") Long id,
                                                                                  @Param("invoiceId") Long invoiceId,
                                                                                  @Param("canceled") boolean canceled,
                                                                                  @Param("state") Enumeration.PAYMENT_STATE state);

    List<InvoicePayment> findAllByIdGreaterThanAndInvoiceIdAndCanceledAndStateEquals(@Param("id") Long id,
                                                                                     @Param("invoiceId") Long invoiceId,
                                                                                     @Param("canceled") boolean canceled,
                                                                                     @Param("state") Enumeration.PAYMENT_STATE state);
}

