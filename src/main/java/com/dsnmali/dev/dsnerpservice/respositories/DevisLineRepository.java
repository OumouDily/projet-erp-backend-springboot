package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.DevisLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DevisLineRepository extends JpaRepository<DevisLine, Long> {

    @Query("SELECT ligne FROM DevisLine ligne WHERE ligne.devis.id=:id")
    List<DevisLine> findByDevis(@Param(value = "id") Long id);

    @Query("SELECT ligne FROM DevisLine ligne WHERE ligne.devis.id=:id")
    DevisLine findDevisLine(@Param(value = "id") Long id);
}
