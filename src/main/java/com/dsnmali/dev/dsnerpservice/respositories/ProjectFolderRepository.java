package com.dsnmali.dev.dsnerpservice.respositories;


import com.dsnmali.dev.dsnerpservice.entities.ProjectFolder;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;


public interface ProjectFolderRepository extends JpaRepository<ProjectFolder, Long> {

    ProjectFolder findProjectFolderById(@Param("id") Long id);

    List<ProjectFolder> findAllByCreateByIdOrAgentId(@Param("createById") Long userId, @Param("agentId") Long agentId);

    List<ProjectFolder> findAllByAgentId(@Param("agentId") Long agentId);

    List<ProjectFolder> findAllByCustomerId(@Param("customerId") Long customerId);

    List<ProjectFolder> findAllByCustomerIdAndAgentId(@Param("customerId") Long customerId, @Param("agentId") Long agentId);


    List<ProjectFolder> findAllByStateIsNotAndAgentId(@Param(value = "state") Enumeration.PROJECT_FOLDER_STATE state,
                                                      @Param("agentId") long agentId);

    @Query("SELECT p from ProjectFolder p where p.state<>:state and p.agent.id=:agentId and " +
            "DATE_FORMAT(p.startDate, '%Y') = DATE_FORMAT(:year, '%Y')")
    List<ProjectFolder> findAllByStateIsNotAndAgentIdAndYear(@Param(value = "state") Enumeration.PROJECT_FOLDER_STATE state,
                                                             @Param("agentId") long agentId,
                                                             @Param("year") Date year);

    @Query("SELECT p from ProjectFolder p where p.state<>:state and p.agent.id=:agentId and " +
            "(DATE_FORMAT(p.startDate, '%Y%m%d') between DATE_FORMAT(:startDate, '%Y%m%d') and DATE_FORMAT(:endDate, '%Y%m%d'))")
    List<ProjectFolder> findAllByStateIsNotAndAgentIdAndDates(@Param(value = "state") Enumeration.PROJECT_FOLDER_STATE state,
                                                              @Param("agentId") long agentId,
                                                              @Param("startDate") Date startDate, @Param("endDate") Date endDate);

}
