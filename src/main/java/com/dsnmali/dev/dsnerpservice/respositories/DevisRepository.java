package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Devis;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface DevisRepository extends JpaRepository<Devis, Long> {

    Devis findDevisById(@Param("id") Long id);

    Page<Devis> findAllDevisByEnabled(boolean enabled, Pageable pageable);

    boolean existsByNumber(String number);

    List<Devis> findAllByInvoiceGenerated(boolean invoiceGenerated);

    List<Devis> findAllByStateIsNot(@Param(value = "state") Enumeration.DEVIS_STATE state);

    List<Devis> findAllByStateIsNotAndCreateById(@Param(value = "state") Enumeration.DEVIS_STATE state,
                                                 @Param("createById") Long createById);

    List<Devis> findAllByAgentId(@Param("agentId") Long agentId);

    List<Devis> findAllByCustomerId(@Param("customerId") Long customerId);

    List<Devis> findAllByState(@Param("state") Enumeration.DEVIS_STATE state);

    List<Devis> findAllByCustomerIdAndAgentId(@Param("customerId") Long customerId, @Param("agentId") Long agentId);

    List<Devis> findAllByAgentIdAndState(@Param("agentId") Long agentId,
                                         @Param("state") Enumeration.DEVIS_STATE state);

    List<Devis> findAllByCustomerIdAndState(@Param("customerId") Long customerId,
                                            @Param("state") Enumeration.DEVIS_STATE state);

    List<Devis> findAllByCustomerIdAndAgentIdAndState(@Param("customerId") Long customerId,
                                                      @Param("agentId") Long agentId,
                                                      @Param("state") Enumeration.DEVIS_STATE state);

    @Query("SELECT d from Devis d where DATE_FORMAT(d.devisDate, '%Y') = DATE_FORMAT(:year, '%Y') and d.state<>:state and d.enabled=true")
    List<Devis> findAllByStateIsNotAndYear(@Param("year") Date year, @Param("state") Enumeration.DEVIS_STATE state);

    @Query("SELECT d from Devis d where d.enabled=true and d.state<>:state and " +
            "(DATE_FORMAT(d.devisDate, '%Y%m%d') between DATE_FORMAT(:startDate, '%Y%m%d') and DATE_FORMAT(:endDate, '%Y%m%d'))")
    List<Devis> findAllByStateIsNotAndDates(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                                            @Param("state") Enumeration.DEVIS_STATE state);

    List<Devis> findAllByStateIsNotAndAgentId(@Param(value = "state") Enumeration.DEVIS_STATE state,
                                              @Param("agentId") Long agentId);

    @Query("SELECT d from Devis d where d.state<>:state and d.agent.id=:agentId and d.enabled=true and " +
            "DATE_FORMAT(d.devisDate, '%Y') = DATE_FORMAT(:year, '%Y')")
    List<Devis> findAllByStateIsNotAndAgentIdAndYear(@Param("state") Enumeration.DEVIS_STATE state,
                                                     @Param("agentId") Long agentId,
                                                     @Param("year") Date year);

    @Query("SELECT d from Devis d where d.state<>:state and d.agent.id=:agentId and d.enabled=true and " +
            "(DATE_FORMAT(d.devisDate, '%Y%m%d') between DATE_FORMAT(:startDate, '%Y%m%d') and DATE_FORMAT(:endDate, '%Y%m%d'))")
    List<Devis> findAllByStateIsNotAndAgentIdAndDates(@Param("state") Enumeration.DEVIS_STATE state,
                                                      @Param("agentId") Long agentId,
                                                      @Param("startDate") Date startDate, @Param("endDate") Date endDate);


}
