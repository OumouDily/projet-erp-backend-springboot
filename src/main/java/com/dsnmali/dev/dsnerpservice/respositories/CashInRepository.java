package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.CashIn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CashInRepository extends JpaRepository<CashIn, Long> {
}
