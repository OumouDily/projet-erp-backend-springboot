package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Task;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findAllByState(@Param("state") Enumeration.TASK_STATE state);

    List<Task> findAllByPriority(@Param("priority") Enumeration.TASK_PRIORITY priority);

    List<Task> findAllByAssignToId(@Param("assignToId") Long assignToId);

    List<Task> findAllByCustomerId(@Param("customerId") Long customerId);

    List<Task> findAllByStateAndAssignToId(@Param("state") Enumeration.TASK_STATE state,
                                           @Param("assignToId") Long assignToId);

    List<Task> findAllByPriorityAndAssignToId(@Param("priority") Enumeration.TASK_PRIORITY priority,
                                              @Param("assignToId") Long assignToId);

    List<Task> findAllByStateAndCustomerId(@Param("state") Enumeration.TASK_STATE state,
                                           @Param("customerId") Long customerId);

    List<Task> findAllByPriorityAndCustomerId(@Param("priority") Enumeration.TASK_PRIORITY priority,
                                              @Param("customerId") Long customerId);

    List<Task> findAllByAssignToIdAndCustomerId(@Param("assignToId") Long assignToId, @Param("customerId") Long customerId);

    List<Task> findAllByStateAndAssignToIdAndCustomerId(@Param("state") Enumeration.TASK_STATE state,
                                                        @Param("assignToId") Long assignToId, @Param("customerId") Long customerId);

    List<Task> findAllByPriorityAndAssignToIdAndCustomerId(@Param("priority") Enumeration.TASK_PRIORITY priority,
                                                           @Param("assignToId") Long assignToId, @Param("customerId") Long customerId);

}
