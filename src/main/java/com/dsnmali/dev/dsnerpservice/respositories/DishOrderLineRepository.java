package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.DishOrderLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishOrderLineRepository extends JpaRepository<DishOrderLine, Long> {
}
