package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.CashDesk;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CashDeskRepository extends JpaRepository<CashDesk, Long> {
}
