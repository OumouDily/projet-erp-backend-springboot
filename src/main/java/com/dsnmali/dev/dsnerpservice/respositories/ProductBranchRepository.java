package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.ProductBranch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductBranchRepository extends JpaRepository<ProductBranch, Long> {
}
