package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    boolean existsByName(String name);

    Role findByName(String name);
}
