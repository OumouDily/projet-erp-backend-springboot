package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.DevisLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DevisLogRepository extends JpaRepository<DevisLog, Long> {
}
