package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.InvoiceLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InvoiceLineRepository extends JpaRepository<InvoiceLine, Long> {

    @Query("SELECT ligne FROM InvoiceLine ligne WHERE ligne.invoice.id=:id")
    List<InvoiceLine> findByInvoice(@Param(value = "id") Long id);

    @Query("SELECT ligne FROM InvoiceLine ligne WHERE ligne.invoice.id=:id")
    InvoiceLine findInvoiceLine(@Param(value = "id") Long id);
}
