package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query("SELECT p FROM Project p WHERE p.invoice.customer.id=:id")
    List<Project> findAllProjectByCustomerId(@Param(value = "id") Long id);

    boolean existsByInvoiceId(@Param(value = "id") Long id);

    List<Project> findByInvoiceId(@Param(value = "id") Long id);
}
