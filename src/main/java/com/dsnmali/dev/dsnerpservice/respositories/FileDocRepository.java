package com.dsnmali.dev.dsnerpservice.respositories;

import com.dsnmali.dev.dsnerpservice.entities.FileDoc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileDocRepository extends JpaRepository<FileDoc, Long> {
}
