package com.dsnmali.dev.dsnerpservice.wrapper;

import com.dsnmali.dev.dsnerpservice.entities.Invoice;
import com.dsnmali.dev.dsnerpservice.entities.InvoiceLine;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceSaveEntity {
    private Invoice invoice;
    private List<InvoiceLine> invoiceLines = new ArrayList<>();
}
