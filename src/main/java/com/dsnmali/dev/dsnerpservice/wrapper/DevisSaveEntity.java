package com.dsnmali.dev.dsnerpservice.wrapper;

import com.dsnmali.dev.dsnerpservice.entities.Devis;
import com.dsnmali.dev.dsnerpservice.entities.DevisLine;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DevisSaveEntity {
    private Devis devis;
    private List<DevisLine> devisLines = new ArrayList<>();
}
