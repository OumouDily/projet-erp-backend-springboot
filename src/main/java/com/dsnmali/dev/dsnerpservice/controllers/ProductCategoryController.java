package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.ProductCategory;
import com.dsnmali.dev.dsnerpservice.services.ProductCategoryService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/product-categories")
@Api(tags = {"ProductCategory - Categorie Produit"})
public class ProductCategoryController {

    final ProductCategoryService productCategoryService;

    public ProductCategoryController(ProductCategoryService productCategoryService) {
        this.productCategoryService = productCategoryService;
    }


    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody ProductCategory productCategory) {
        return ResponseEntity.ok(productCategoryService.save(productCategory));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody ProductCategory productCategory) {
        return ResponseEntity.ok(productCategoryService.edit(productCategory));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return ResponseEntity.ok(productCategoryService.delete(id));
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(productCategoryService.getAll());
    }

    @GetMapping("/{productCategoryId}/get")
    @Transactional(readOnly = true)
    public ResponseEntity<?> get(@PathVariable("productCategoryId") Long productCategoryId) {
        return ResponseEntity.ok(productCategoryService.getById(productCategoryId));
    }

}
