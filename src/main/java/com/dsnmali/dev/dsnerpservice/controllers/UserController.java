package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.User;
import com.dsnmali.dev.dsnerpservice.services.UserService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

/**/
@RestController
@RequestMapping("/api/users")
@Api(tags = {"User - Utilisateur"})
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> listOfUsers() {
        return ResponseEntity.ok(userService.listOfUsers());
    }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody User user) {
        return ResponseEntity.ok(userService.create(user));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody User user) {
        return ResponseEntity.ok(userService.edit(user));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return ResponseEntity.ok(userService.remove(id));
    }

    @GetMapping("/{id}/getUser")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUser(id));
    }


}
