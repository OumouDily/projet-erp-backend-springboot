package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.InvoiceLineService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/invoice-lines")
@Api(tags = {"InvoiceLine - Line Facture"})
public class InvoiceLineController {

    final InvoiceLineService invoiceLineService;

    public InvoiceLineController(InvoiceLineService invoiceLineService) {
        this.invoiceLineService = invoiceLineService;
    }

    @GetMapping("find-by-invoice/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findByInvoice(@PathVariable Long id) {
        return ResponseEntity.ok(invoiceLineService.findListByInvoice(id));
    }

    @GetMapping("find-line/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findInvoiceLine(@PathVariable Long id) {
        return ResponseEntity.ok(invoiceLineService.findInvoiceLine(id));
    }
}
