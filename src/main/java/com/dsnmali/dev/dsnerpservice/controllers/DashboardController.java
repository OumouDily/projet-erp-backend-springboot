package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.CustomerService;
import com.dsnmali.dev.dsnerpservice.services.DashboardService;
import com.dsnmali.dev.dsnerpservice.services.InvoiceService;
import com.dsnmali.dev.dsnerpservice.services.ProjectFolderService;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/dashboards")
@Api(tags = {"Invoice - Facture"})
public class DashboardController {

    final DashboardService dashboardService;
    final InvoiceService invoiceService;
    final CustomerService customerService;
    final ProjectFolderService projectFolderService;

    public DashboardController(DashboardService dashboardService, InvoiceService invoiceService,
                               CustomerService customerService, ProjectFolderService projectFolderService) {
        this.dashboardService = dashboardService;
        this.invoiceService = invoiceService;
        this.customerService = customerService;
        this.projectFolderService = projectFolderService;
    }


    @PostMapping("/invoice")
    @Transactional(readOnly = true)
    public ResponseEntity<?> initInvoiceDash(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(invoiceService.initInvoiceDash(searchBody));
    }

    @PostMapping("/crm")
    @Transactional(readOnly = true)
    public ResponseEntity<?> initCrmDash(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(customerService.initCrmDash(searchBody));
    }

    @PostMapping("/project-folder")
    @Transactional(readOnly = true)
    public ResponseEntity<?> initProjectFolderDash(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(projectFolderService.initProjectFolderDash(searchBody));
    }

    @PostMapping("/commercial")
    @Transactional(readOnly = true)
    public ResponseEntity<?> initCommercialDash(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(dashboardService.initCommercialDash(searchBody));
    }

}
