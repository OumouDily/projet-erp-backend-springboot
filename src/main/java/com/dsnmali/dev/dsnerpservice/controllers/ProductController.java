package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.Product;
import com.dsnmali.dev.dsnerpservice.services.ProductService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/products")
@Api(tags = {"Product - Produit"})
public class ProductController {
    private final
    ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody Product product) {
        return ResponseEntity.ok(productService.save(product));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody Product product) {
        return ResponseEntity.ok(productService.edit(product));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return ResponseEntity.ok(productService.delete(id));
    }

    @GetMapping("/all/{categoryId}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getAll(@PathVariable("categoryId") Long categoryId) {
        return ResponseEntity.ok(productService.getAll(categoryId));
    }

    @GetMapping("/{productId}/get")
    @Transactional(readOnly = true)
    public ResponseEntity<?> get(@PathVariable("productId") Long productId) {
        return ResponseEntity.ok(productService.getById(productId));
    }
}
