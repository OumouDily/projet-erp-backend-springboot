package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.AuthService;
import com.dsnmali.dev.dsnerpservice.utils.AuthBody;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**/
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @ApiOperation("Login user")
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthBody authBody) {
        return new ResponseEntity<>(authService.login(authBody.getUsername(), authBody.getPassword()), HttpStatus.OK);
    }

    @ApiOperation("Update user's password")
    @PostMapping("/update_pwd")
    public ResponseEntity<?> updatePwd(@RequestBody AuthBody authBody) {
        return new ResponseEntity<>(authService.updatePassword(authBody), HttpStatus.OK);
    }

    @ApiOperation("Reset user's password")
    @PostMapping("/reset_pwd")
    public ResponseEntity<?> resetPwd(@RequestBody AuthBody authBody) {
        return new ResponseEntity<>(authService.resetPassword(authBody.getUserId(), authBody.getNewPassword()), HttpStatus.OK);
    }

    @ApiOperation("Identify who is calling")
    @GetMapping("/who_is")
    public ResponseEntity<?> whois(HttpServletRequest request) {
        return new ResponseEntity<>(authService.whoIs(request), HttpStatus.OK);
    }

    @ApiOperation("Refresh user token")
    @GetMapping("/{username}/refresh_token")
    public ResponseEntity<?> refreshToken(@PathVariable(value = "username") String username) {
        return new ResponseEntity<>(authService.refreskToken(username), HttpStatus.OK);
    }
}
