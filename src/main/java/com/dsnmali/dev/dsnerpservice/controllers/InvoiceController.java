package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.Invoice;
import com.dsnmali.dev.dsnerpservice.services.InvoiceService;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import com.dsnmali.dev.dsnerpservice.wrapper.InvoiceSaveEntity;
import io.swagger.annotations.Api;

import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/invoices")
@Api(tags = {"Invoice - Facture"})
public class InvoiceController {

    final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody InvoiceSaveEntity invoiceSaveEntity) {
        return ResponseEntity.ok(invoiceService.createInvoice(invoiceSaveEntity));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody InvoiceSaveEntity invoiceSaveEntity) {
        return ResponseEntity.ok(invoiceService.updateInvoice(invoiceSaveEntity));
    }

    @PutMapping("/an-validate")
    public ResponseEntity<?> validateAnInvoice(@RequestBody Invoice invoice) {
        return ResponseEntity.ok(invoiceService.validateAnInvoice(invoice));
    }

    @PutMapping("/cancel")
    public ResponseEntity<?> cancelInvoice(@RequestBody Invoice invoice) {
        return ResponseEntity.ok(invoiceService.cancelInvoice(invoice));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> deleteInvoice(@PathVariable("id") Long id) {
        return ResponseEntity.ok(invoiceService.deleteInvoice(id));
    }

    @GetMapping("/not-paid")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAllInvoiceNotPaid() {
        return ResponseEntity.ok(invoiceService.findAllInvoiceNotPaid());
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(invoiceService.findAll());
    }


    @PostMapping("/list")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAllInvoice(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(invoiceService.list(searchBody));
    }

    @GetMapping("/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(invoiceService.getById(id));
    }

    @GetMapping("/getByPaid/{paid}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getByPaid(@PathVariable(value = "paid") boolean paid) {
        return ResponseEntity.ok(invoiceService.getInvoicesByPaid(paid));
    }

    @PostMapping("/dashboard")
    @Transactional(readOnly = true)
    public ResponseEntity<?> initInvoiceDash(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(invoiceService.initInvoiceDash(searchBody));
    }

    @GetMapping("/details/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getDetailsById(@PathVariable Long id) {
        return ResponseEntity.ok(invoiceService.getById(id));
    }

}
