package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.ProjectFolder;
import com.dsnmali.dev.dsnerpservice.services.ProjectFolderService;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/project-folder")
@Api(tags = {"Project Folder - Dossier projet"})
public class ProjectFolderController {

    private final ProjectFolderService projectFolderService;

    public ProjectFolderController(ProjectFolderService projectFolderService) {
        this.projectFolderService = projectFolderService;
    }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody ProjectFolder folder) {
        return ResponseEntity.ok(projectFolderService.save(folder));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody ProjectFolder folder) {
        return ResponseEntity.ok(projectFolderService.edit(folder));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return ResponseEntity.ok(projectFolderService.deleteById(id));
    }

    @PostMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> list(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(projectFolderService.list(searchBody));
    }


    @GetMapping("/dashboard")
    @Transactional(readOnly = true)
    public ResponseEntity<?> initProjectFolderDash(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(projectFolderService.initProjectFolderDash(searchBody));
    }


    @GetMapping("/{id}/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(projectFolderService.getById(id));
    }

}
