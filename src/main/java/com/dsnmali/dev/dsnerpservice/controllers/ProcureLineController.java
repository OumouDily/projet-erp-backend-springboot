package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.ProcurementLineService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/procurements-lines")
@Api(tags = {"ProcurementLine - Ligne Appro"})
public class ProcureLineController {

    final ProcurementLineService procurementLineService;

    public ProcureLineController(ProcurementLineService procurementLineService) {
        this.procurementLineService = procurementLineService;
    }
}
