package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.ProductBranchService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/product-branchs")
@Api(description = "ProductBranch - Produit Secteur")
public class ProductBranchController {
    private final
    ProductBranchService productBranchService;

    public ProductBranchController(ProductBranchService productBranchService) {
        this.productBranchService = productBranchService;
    }
}

