package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.LogService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/logs")
@Api(tags = {"Log - Historique"})
public class LogController {

    final LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }
}
