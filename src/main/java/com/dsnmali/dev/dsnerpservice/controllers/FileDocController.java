package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.FileDocService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/filedocs")
@Api(tags = {"FileDoc - Document Fichier"})
public class FileDocController {

    final FileDocService fileDocService;

    public FileDocController(FileDocService fileDocService) {
        this.fileDocService = fileDocService;
    }
}
