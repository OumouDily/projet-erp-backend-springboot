package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.CashOutDetailsService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/cashsout-details")
@Api(tags = {"CashOutDetails - Details Sortie Caisse"})
public class CashOutDetailsController {

    final CashOutDetailsService cashOutDetailsService;

    public CashOutDetailsController(CashOutDetailsService cashOutDetailsService) {
        this.cashOutDetailsService = cashOutDetailsService;
    }
}
