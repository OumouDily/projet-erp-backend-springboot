package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.Task;
import com.dsnmali.dev.dsnerpservice.services.TaskService;
import com.dsnmali.dev.dsnerpservice.utils.Enumeration;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tasks")
@Api(tags = {"Task - Tache"})
public class TaskController {
    private final
    TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/find")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAll(@RequestBody SearchBody searchBody) {
        System.out.println("searchBody : " + searchBody);
        return ResponseEntity.ok(taskService.find(searchBody));
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(taskService.getAll());
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody Task task) {
        return ResponseEntity.ok(taskService.save(task));
    }

    @PutMapping("/update")
    public ResponseEntity<?> edit(@RequestBody Task task) {
        return ResponseEntity.ok(taskService.edit(task));
    }

    @GetMapping("/all/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(taskService.getById(id));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return ResponseEntity.ok(taskService.remove(id));
    }

    @PutMapping("/updating-by-state/{state}")
    public ResponseEntity<?> updateByState(@PathVariable(value = "state") Enumeration.TASK_STATE state, @RequestBody Task task) {
        return ResponseEntity.ok(taskService.updateByState(state, task));
    }

    @PutMapping("/updating-by-priority/{priority}")
    public ResponseEntity<?> updateByPriority(@PathVariable(value = "priority") Enumeration.TASK_PRIORITY priority, @RequestBody Task task) {
        return ResponseEntity.ok(taskService.updateByPriority(priority, task));
    }
}
