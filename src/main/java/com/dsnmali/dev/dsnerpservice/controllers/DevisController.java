package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.Devis;
import com.dsnmali.dev.dsnerpservice.services.DevisService;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import com.dsnmali.dev.dsnerpservice.wrapper.DevisSaveEntity;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/devis")
@Api(tags = {"Devis - Devis"})
public class DevisController {

    private final DevisService devisService;

    public DevisController(DevisService devisService) {
        this.devisService = devisService;
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(devisService.findAll());
    }

    @PostMapping("/find-all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAllDevis(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(devisService.list(searchBody));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(devisService.getById(id));
    }

    @GetMapping("/details/{id}")
    public ResponseEntity<?> getDetailsById(@PathVariable Long id) {
        return ResponseEntity.ok(devisService.getById(id));
    }

    @GetMapping("/getByGenereted/{generated}")
    public ResponseEntity<?> getByGenerated(@PathVariable(value = "generated") boolean generated) {
        return ResponseEntity.ok(devisService.getDevisByGenerated(generated));
    }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody DevisSaveEntity devisSaveEntity) {
        return ResponseEntity.ok(devisService.createDevis(devisSaveEntity));
    }

    @PostMapping("/duplicate")
    public ResponseEntity<?> duplicate(@RequestBody DevisSaveEntity devisSaveEntity) {
        return ResponseEntity.ok(devisService.duplicate(devisSaveEntity));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody DevisSaveEntity devisSaveEntity) {
        return ResponseEntity.ok(devisService.updateDevis(devisSaveEntity));
    }

    @PutMapping("/cancel")
    public ResponseEntity<?> deleteDevis(@RequestBody Devis devis) {
        return ResponseEntity.ok(devisService.cancelDevis(devis));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> deleteDevis(@PathVariable("id") Long id) {
        return ResponseEntity.ok(devisService.deleteDevis(id));
    }

}
