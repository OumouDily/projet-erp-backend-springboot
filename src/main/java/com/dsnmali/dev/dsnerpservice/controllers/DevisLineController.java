package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.DevisLineService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/devis-lines")
@Api(tags = {"DevisLine - Ligne Devis"})
public class DevisLineController {

    private final DevisLineService devisLineService;

    public DevisLineController(DevisLineService devisLineService) {
        this.devisLineService = devisLineService;
    }

    @GetMapping("find-by-devis/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findListByDevis(@PathVariable Long id) {
        return ResponseEntity.ok(devisLineService.findListByDevis(id));
    }

    @GetMapping("find-line/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findDevisLine(@PathVariable Long id) {
        return ResponseEntity.ok(devisLineService.findDevisLine(id));
    }
}
