package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.Customer;
import com.dsnmali.dev.dsnerpservice.services.CustomerService;
import com.dsnmali.dev.dsnerpservice.utils.SearchBody;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/customers")
@Api(tags = {"Customer - Client Prospect"})
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.save(customer));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody Customer customer) {
        return ResponseEntity.ok(customerService.edit(customer));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return ResponseEntity.ok(customerService.deleteById(id));
    }

    @PostMapping("/prospects")
    public ResponseEntity<?> getAllProspect(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(customerService.getAllProspect(searchBody));
    }

    @PostMapping("/clients")
    public ResponseEntity<?> getAllCustomer(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(customerService.getAllCustomer(searchBody));
    }

    @PostMapping("/find")
    @Transactional(readOnly = true)
    public ResponseEntity<?> listOfProspectClientByUser(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(customerService.findProspectCustomer(searchBody));
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(customerService.findAll());
    }

    @GetMapping("/{id}/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(customerService.getById(id));
    }

    @PostMapping("/allCustomers")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAllClient(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(customerService.findAllClient(searchBody));
    }

    @PostMapping("/allProspects")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAllProspect(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(customerService.findAllProspect(searchBody));
    }

    @PostMapping("/dashboard")
    @Transactional(readOnly = true)
    public ResponseEntity<?> initCrmDash(@RequestBody SearchBody searchBody) {
        return ResponseEntity.ok(customerService.initCrmDash(searchBody));
    }


}
