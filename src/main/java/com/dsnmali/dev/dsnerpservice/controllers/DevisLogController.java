package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.DevisLogService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/quotes")
@Api(tags = {"DevisLog - Historique Devis"})
public class DevisLogController {

    final DevisLogService devisLogService;

    public DevisLogController(DevisLogService devisLogService) {
        this.devisLogService = devisLogService;
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAllQuote() {
        return ResponseEntity.ok(devisLogService.findAllQuote());
    }
}
