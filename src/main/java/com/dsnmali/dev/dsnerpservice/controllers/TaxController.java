package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.Tax;
import com.dsnmali.dev.dsnerpservice.services.TaxService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/taxes")
@Api(tags = {"Tax - Taxe"})
public class TaxController {
    private final TaxService taxService;

    public TaxController(TaxService taxService) {
        this.taxService = taxService;
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(taxService.findAll());
    }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody Tax tax) {
        return ResponseEntity.ok(taxService.save(tax));
    }


    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody Tax tax) {
        return ResponseEntity.ok(taxService.edit(tax));
    }

    @GetMapping("/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getSector(@PathVariable Long id) {
        return ResponseEntity.ok(taxService.getById(id));
    }

}
