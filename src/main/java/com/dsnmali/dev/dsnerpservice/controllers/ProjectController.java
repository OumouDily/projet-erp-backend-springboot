package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.Project;
import com.dsnmali.dev.dsnerpservice.services.ProjectService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/projects")
@Api(tags = {"Project - Projet"})
public class ProjectController {
    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAll() {
        System.out.println("Frontend.....");
        return ResponseEntity.ok(projectService.getAll());
    }

    @GetMapping("/by-invoiceId/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAllByInvoiceId(@PathVariable Long id) {
        return ResponseEntity.ok(projectService.findAllByInvoiceId(id));
    }

    @PostMapping("/save")
    public ResponseEntity<?> save(@RequestBody Project project) {
        System.out.println(project.toString()
        );
        return ResponseEntity.ok(projectService.save(project));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody Project project) {
        return ResponseEntity.ok(projectService.edit(project));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> remove(@PathVariable Long id) {
        return ResponseEntity.ok(projectService.remove(id));
    }

    @GetMapping("/all/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.ok(projectService.getById(id));
    }


    @GetMapping("/by-customer/{id}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAllProjectByCustomerId(@PathVariable Long id) {
        return ResponseEntity.ok(projectService.findAllProjectByCustomerId(id));
    }


}
