package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.CashDeskService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cashs")
@Api(tags = {"CashDesk - Caisse"})
public class CashDeskController {

    final CashDeskService cashDeskService;

    public CashDeskController(CashDeskService cashDeskService) {
        this.cashDeskService = cashDeskService;
    }
}
