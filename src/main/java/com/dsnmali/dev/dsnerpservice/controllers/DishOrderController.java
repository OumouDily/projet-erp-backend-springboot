package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.DishOrderService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/dishorders")
@Api(tags = {"Dish Order - Commande Plat"})
public class DishOrderController {

    final DishOrderService dishOrderService;

    public DishOrderController(DishOrderService dishOrderService) {
        this.dishOrderService = dishOrderService;
    }
}
