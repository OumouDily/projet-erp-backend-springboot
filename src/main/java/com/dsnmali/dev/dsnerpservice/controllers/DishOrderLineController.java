package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.DishOrderLineService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/dishorders-lines")
@Api(tags = {"DishOrderLine - Ligne Commande Plat"})
public class DishOrderLineController {

    final DishOrderLineService dishOrderLineService;

    public DishOrderLineController(DishOrderLineService dishOrderLineService) {
        this.dishOrderLineService = dishOrderLineService;
    }
}
