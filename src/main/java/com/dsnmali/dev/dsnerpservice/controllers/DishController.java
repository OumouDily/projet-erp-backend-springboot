package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.DishService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/dishes")
@Api(tags = {"Dish - Plat"})
public class DishController {
    private final
    DishService dishService;

    public DishController(DishService dishService) {
        this.dishService = dishService;
    }
}
