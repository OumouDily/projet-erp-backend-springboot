package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.BranchActivity;
import com.dsnmali.dev.dsnerpservice.services.BranchActivityService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/branch-activities")
@Api(tags = {"BranchActivity - Sector Activite "})
public class BranchActivityController {

    private final BranchActivityService branchActivityService;

    public BranchActivityController(BranchActivityService branchActivityService) {
        this.branchActivityService = branchActivityService;
    }


    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> listOfSectorOfActivities() {
        return ResponseEntity.ok(branchActivityService.listOfSectorOfActivities());
    }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody BranchActivity branchActivity) {
        return ResponseEntity.ok(branchActivityService.save(branchActivity));
    }


    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody BranchActivity branchActivity) {
        return ResponseEntity.ok(branchActivityService.edit(branchActivity));
    }

    @GetMapping("/{id}/getBranchActivity")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getSector(@PathVariable Long id) {
        return ResponseEntity.ok(branchActivityService.getById(id));
    }

    @GetMapping("/by-name")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getSectorObjFromName(@RequestParam String name) {
        return ResponseEntity.ok(branchActivityService.getSectorObjFromName(name));
    }
}
