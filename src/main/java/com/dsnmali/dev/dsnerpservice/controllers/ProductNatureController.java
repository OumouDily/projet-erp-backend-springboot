package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.services.ProductNatureService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/products-nature")
@Api(tags = {"NatureProduct - Nature Produit"})
public class ProductNatureController {
    private final
    ProductNatureService productNatureService;

    public ProductNatureController(ProductNatureService productNatureService) {
        this.productNatureService = productNatureService;
    }
}
