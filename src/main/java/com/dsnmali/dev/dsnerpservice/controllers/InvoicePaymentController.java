package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.InvoicePayment;
import com.dsnmali.dev.dsnerpservice.services.InvoicePaymentService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/payments")
@Api(tags = {"InvoicePayment - Paiement Facture"})
public class InvoicePaymentController {

    final InvoicePaymentService invoicePaymentService;

    public InvoicePaymentController(InvoicePaymentService invoicePaymentService) {
        this.invoicePaymentService = invoicePaymentService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody InvoicePayment invoicePayment) {
        return ResponseEntity.ok(invoicePaymentService.create(invoicePayment));
    }

    @PostMapping("/validate")
    public ResponseEntity<?> save(@RequestBody InvoicePayment invoicePayment) {
        return ResponseEntity.ok(invoicePaymentService.validate(invoicePayment));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody InvoicePayment invoicePayment) {
        return ResponseEntity.ok(invoicePaymentService.update(invoicePayment));
    }

    @PutMapping("/cancel")
    public ResponseEntity<?> cancel(@RequestBody InvoicePayment invoicePayment) {
        return ResponseEntity.ok(invoicePaymentService.cancelPayment(invoicePayment));
    }

    @GetMapping("/all")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(invoicePaymentService.getAll());
    }

    @GetMapping("/{id}/get")
    @Transactional(readOnly = true)
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return ResponseEntity.ok(invoicePaymentService.getById(id));
    }
}
