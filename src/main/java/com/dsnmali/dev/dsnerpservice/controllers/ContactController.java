package com.dsnmali.dev.dsnerpservice.controllers;

import com.dsnmali.dev.dsnerpservice.entities.Contact;
import com.dsnmali.dev.dsnerpservice.services.ContactService;
import io.swagger.annotations.Api;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/contacts")
@Api(tags = {"Contact - Contact Client"})
public class ContactController {

    private final ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @PostMapping("/save")
    public ResponseEntity<?> create(@RequestBody Contact contact) {
        return ResponseEntity.ok(contactService.save(contact));
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody Contact contact) {
        return ResponseEntity.ok(contactService.edit(contact));
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        return ResponseEntity.ok(contactService.delete(id));
    }

    @GetMapping("/all/{customerId}")
    @Transactional(readOnly = true)
    public ResponseEntity<?> getAll(@PathVariable("customerId") Long customerId) {
        return ResponseEntity.ok(contactService.getAll(customerId));
    }

    @GetMapping("/{contactId}/get")
    @Transactional(readOnly = true)
    public ResponseEntity<?> get(@PathVariable("contactId") Long contactId) {
        return ResponseEntity.ok(contactService.getById(contactId));
    }

}
